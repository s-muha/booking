package by.itstep.booking.service;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryShortDto;
import by.itstep.booking.dto.country.CountryUpdateDto;

import java.util.List;

public interface CountryService {

    CountryFullDto findById(Integer id);

    List<CountryShortDto> findAll();

    CountryFullDto create(CountryCreateDto countryCreateDto);

    CountryFullDto update(CountryUpdateDto countryUpdateDto);

    void deleteById(Integer id);
}
