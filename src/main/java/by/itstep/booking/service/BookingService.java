package by.itstep.booking.service;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.dto.booking.BookingShortDto;
import by.itstep.booking.dto.booking.BookingUpdateDto;

import java.util.List;

public interface BookingService {

    BookingFullDto findById(Integer id);

    List<BookingShortDto> findAll();

    BookingFullDto create(BookingCreateDto bookingCreateDto);

    BookingFullDto update(BookingUpdateDto bookingUpdateDto);

    void deleteById(Integer id);
}
