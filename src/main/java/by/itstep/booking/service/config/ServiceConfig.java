package by.itstep.booking.service.config;

import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.enums.UserRole;
import by.itstep.booking.exeption.WrongBookingDateException;
import by.itstep.booking.security.JwtAuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;

@Service
public class ServiceConfig {

    public Double calculateCostBooking(Date startDate, Date endDate, RoomEntity room, HotelEntity hotel) {
        long millisecondsInOneDay = 86400000;
        int numberOfDaysOfBooking = (int) ((endDate.getTime() - startDate.getTime()) / millisecondsInOneDay + 1);
        return hotel.getPrice() * numberOfDaysOfBooking * room.getNumberOfSeatsAndFactor().getCostFactor();
    }

    public boolean currentUserIsAdmin() {
        return SecurityContextHolder.getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .anyMatch(ga -> ga.getAuthority().equals("ROLE_" + UserRole.ADMIN.name()));
    }

    public void throwIfNotCurrentUserOrAdmin(String email) {
        if (!currentUserIsAdmin()) {
            String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            if (!currentUserEmail.equals(email)) {
                throw new JwtAuthenticationException("JWT token is expired or invalid");
            }
        }
    }

    public void throwIfNotCurrentUser(String email) {
        String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!currentUserEmail.equals(email)) {
            throw new JwtAuthenticationException("JWT token is expired or invalid");
        }
    }

    public void checkBookingDates(Date startDate, Date endDate) {
        if (startDate.before(Date.valueOf(LocalDate.now()))) {
            throw new WrongBookingDateException("Booking start date cannot be earlier than now");
        }
        if (startDate.after(endDate)) {
            throw new WrongBookingDateException(
                    "The start date of the booking cannot be later than the end date of the booking");
        }
    }

}
