package by.itstep.booking.service;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;

import java.sql.Date;
import java.util.List;

public interface HotelService {

    HotelFullDto findById(Integer id);

    List<HotelShortDto> findAll();

    HotelFullDto create(HotelCreateDto hotelCreateDto);

    HotelFullDto update(HotelUpdateDto hotelUpdateDto);

    void deleteById(Integer id);

    List<HotelShortDto> searchFreeHotelsInCountry(Integer countryId, NumberOfSeatsAndFactor numberOfSeatsAndFactor,
                                                  Date startDate, Date endDate);
}
