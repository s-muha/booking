package by.itstep.booking.service;

import by.itstep.booking.dto.user.*;

import java.util.List;

public interface UserService {

    UserFullDto findById(Integer id);

    List<UserShortDto> findAll();

    UserFullDto create(UserCreateDto userCreateDto);

    UserFullDto update(UserUpdateDto userUpdateDto);

    void deleteById(Integer id);

    void changePassword(ChangePasswordDto changePasswordDto);

    UserFullDto changeRole(ChangeRoleDto changeRoleDto);

    void topUpAccount(Integer userId, Double amountOfMoney);

}
