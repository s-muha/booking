package by.itstep.booking.service;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.room.RoomUpdateDto;

import java.util.List;

public interface RoomService {

    RoomFullDto findById(Integer id);

    List<RoomShortDto> findAll();

    RoomFullDto create(RoomCreateDto roomCreateDto);

    RoomFullDto update(RoomUpdateDto roomUpdateDto);

    void deleteById(Integer id);

}
