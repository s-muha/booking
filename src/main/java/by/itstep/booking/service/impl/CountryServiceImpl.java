package by.itstep.booking.service.impl;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryShortDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.exeption.AppEntityNotFoundException;
import by.itstep.booking.exeption.UniqueValuesIsTakenException;
import by.itstep.booking.mapper.CountryMapper;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.service.CountryService;
import by.itstep.booking.service.config.ServiceConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@Service
@Slf4j
@RequiredArgsConstructor
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;
    private final CountryMapper countryMapper;
    private final ServiceConfig serviceConfig;


    @Override
    @Transactional
    public CountryFullDto findById(Integer id) {
        CountryEntity foundCountry = countryRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("CountryEntity was not found by id: " + id));
        CountryFullDto countryFullDto = countryMapper.map(foundCountry);
        log.info("Country was successfully found");
        return countryFullDto;
    }

    @Override
    @Transactional
    public List<CountryShortDto> findAll() {
        List<CountryEntity> foundCountries = countryRepository.findAll();
        List<CountryShortDto> countryShortDtos = countryMapper.map(foundCountries);
        log.info(countryShortDtos.size() + " countries were found");
        return countryShortDtos;
    }

    @Override
    @Transactional
    public CountryFullDto create(CountryCreateDto countryCreateDto) {
        CountryEntity entityToSave = countryMapper.map(countryCreateDto);

        Optional<CountryEntity> countryWithSameName = countryRepository.findByName(entityToSave.getName());

        if (countryWithSameName.isPresent()) {
            throw new UniqueValuesIsTakenException("Name: " + entityToSave.getName() + " is taken");
        }

        CountryEntity savedCountry = countryRepository.save(entityToSave);
        CountryFullDto countryFullDto = countryMapper.map(savedCountry);
        log.info("Country was successfully created");
        return countryFullDto;
    }

    @Override
    @Transactional
    public CountryFullDto update(CountryUpdateDto countryUpdateDto) {
        CountryEntity countryToUpdated = countryRepository.findById(countryUpdateDto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("CountryEntity was not found by id: " + countryUpdateDto.getId()));
        Optional<CountryEntity> entityWithSameName = countryRepository.findByName(countryUpdateDto.getName());
        if (entityWithSameName.isPresent()) {
            throw new UniqueValuesIsTakenException("Name is taken");
        }
        countryToUpdated.setName(countryUpdateDto.getName());
        CountryEntity updatedCountry = countryRepository.save(countryToUpdated);
        CountryFullDto countryFullDto = countryMapper.map(updatedCountry);
        log.info("Country was successfully updated");
        return countryFullDto;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        CountryEntity countryToDeleted = countryRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("CountryEntity was not found by id: " + id));
        for (HotelEntity hotel : countryToDeleted.getHotels()) {
            for (RoomEntity room : hotel.getRooms()) {
                for (BookingEntity booking : room.getBookings()) {
                    UserEntity user = booking.getUser();
                    if (booking.getStartDate().after(Date.valueOf(LocalDate.now()))) {
                        user.setAmountOfMoney(
                                user.getAmountOfMoney() + (serviceConfig.calculateCostBooking(booking.getStartDate(),
                                        booking.getEndDate(), booking.getRoom(), booking.getRoom().getHotel())));
                    }
                    booking.setUser(user);
                    booking.setDeletedAt(Instant.now());
                }
                room.setDeletedAt(Instant.now());
            }
            hotel.setDeletedAt(Instant.now());
        }
        countryToDeleted.setDeletedAt(Instant.now());
        countryRepository.save(countryToDeleted);
        log.info("Country was successfully deleted");
    }

}
