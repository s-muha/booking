package by.itstep.booking.service.impl;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import by.itstep.booking.exeption.AppEntityNotFoundException;
import by.itstep.booking.exeption.UniqueValuesIsTakenException;
import by.itstep.booking.mapper.HotelMapper;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.service.HotelService;
import by.itstep.booking.service.config.ServiceConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class HotelServiceImpl implements HotelService {

    private final HotelRepository hotelRepository;
    private final HotelMapper hotelMapper;
    private final CountryRepository countryRepository;
    private final RoomRepository roomRepository;
    private final ServiceConfig serviceConfig;


    @Override
    @Transactional
    public HotelFullDto findById(Integer id) {
        HotelEntity foundHotel = hotelRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("HotelEntity was not found by id: " + id));
        HotelFullDto hotelFullDto = hotelMapper.map(foundHotel);
        log.info("Hotel was successfully found");
        return hotelFullDto;
    }

    @Override
    @Transactional
    public List<HotelShortDto> findAll() {
        List<HotelEntity> foundHotels = hotelRepository.findAll();
        List<HotelShortDto> hotelShortDtos = hotelMapper.map(foundHotels);
        log.info(hotelShortDtos.size() + " hotels were found");
        return hotelShortDtos;
    }

    @Override
    @Transactional
    public HotelFullDto create(HotelCreateDto hotelCreateDto) {
        CountryEntity foundCountry = countryRepository.findById(hotelCreateDto.getCountryId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("CountryEntity was not found by id: " + hotelCreateDto.getCountryId()));

        Optional<HotelEntity> hotelWithSameName = hotelRepository
                .findByNameAndCountryId(hotelCreateDto.getName(), hotelCreateDto.getCountryId());
        if (hotelWithSameName.isPresent()) {
            throw new UniqueValuesIsTakenException
                    ("Name hotel: " + hotelCreateDto.getName() + " is taken in given country");
        }
        HotelEntity hotelToSave = hotelMapper.map(hotelCreateDto);
        List<RoomEntity> roomEntities = createRooms(hotelCreateDto);
        hotelToSave.setRooms(roomEntities);

        for (RoomEntity room : roomEntities) {
            room.setHotel(hotelToSave);
        }
        hotelToSave.setCountry(foundCountry);
        hotelToSave.setRating(0.0);
        HotelEntity savedHotel = hotelRepository.save(hotelToSave);
        HotelFullDto hotelFullDto = hotelMapper.map(savedHotel);
        log.info("Hotel was successfully created");
        return hotelFullDto;
    }

    @Override
    @Transactional
    public HotelFullDto update(HotelUpdateDto hotelUpdateDto) {
        HotelEntity hotelToUpdated = hotelRepository.findById(hotelUpdateDto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "HotelEntity was not found by id: " + hotelUpdateDto.getId()));

        Optional<HotelEntity> hotelWithSameName = hotelRepository
                .findByNameAndCountryId(hotelUpdateDto.getName(), hotelToUpdated.getCountry().getId());
        if (hotelWithSameName.isPresent()) {
            throw new UniqueValuesIsTakenException
                    ("Name hotel: " + hotelUpdateDto.getName() + " is taken in given country");
        }

        hotelToUpdated.setName(hotelUpdateDto.getName());
        hotelToUpdated.setAddress(hotelUpdateDto.getAddress());
        hotelToUpdated.setNumberOfStars(hotelUpdateDto.getNumberOfStars());
        hotelToUpdated.setPrice(hotelUpdateDto.getPrice());

        HotelEntity updatedHotel = hotelRepository.save(hotelToUpdated);
        HotelFullDto hotelFullDto = hotelMapper.map(updatedHotel);
        log.info("Hotel was successfully updated");
        return hotelFullDto;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        HotelEntity hotelToDeleted = hotelRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("HotelEntity was not found by id: " + id));
        for (RoomEntity room : hotelToDeleted.getRooms()) {
            for (BookingEntity booking : room.getBookings()) {
                UserEntity user = booking.getUser();
                if (booking.getStartDate().after(Date.valueOf(LocalDate.now()))) {
                    user.setAmountOfMoney(
                            user.getAmountOfMoney() + (serviceConfig.calculateCostBooking(booking.getStartDate(),
                                    booking.getEndDate(), booking.getRoom(), booking.getRoom().getHotel())));
                }
                booking.setUser(user);
                booking.setDeletedAt(Instant.now());
            }
            room.setDeletedAt(Instant.now());
        }
        hotelToDeleted.setDeletedAt(Instant.now());
        hotelRepository.save(hotelToDeleted);
        log.info("Hotel was successfully deleted");
    }

    @Override
    @Transactional
    public List<HotelShortDto> searchFreeHotelsInCountry(
            Integer countryId,
            NumberOfSeatsAndFactor numberOfSeatsAndFactor,
            Date startDate,
            Date endDate) {
        CountryEntity foundCountry = countryRepository.findById(countryId)
                .orElseThrow(() -> new AppEntityNotFoundException("CountryEntity was not found by id: " + countryId));
        serviceConfig.checkBookingDates(startDate, endDate);
        List<HotelEntity> availableHotels = new ArrayList<>();
        for (HotelEntity hotel : foundCountry.getHotels()) {
            if (findAvailableHotels(startDate, endDate, numberOfSeatsAndFactor, hotel)) {
                availableHotels.add(hotel);
            }
        }
        List<HotelShortDto> hotelShortDtos = hotelMapper.map(availableHotels);
        log.info(hotelShortDtos.size() + " hotels were found");
        return hotelShortDtos;
    }

    private List<RoomEntity> createRooms(HotelCreateDto hotelCreateDto) {

        List<RoomEntity> roomEntities = new ArrayList<>();

        for (int i = 0; i < hotelCreateDto.getNumberOfSingleRooms(); i++) {
            RoomEntity room = new RoomEntity();
            room.setNumberOfSeatsAndFactor(NumberOfSeatsAndFactor.ONE_SEAT);
            roomEntities.add(room);
        }
        for (int i = 0; i < hotelCreateDto.getNumberOfDoubleRooms(); i++) {
            RoomEntity room = new RoomEntity();
            room.setNumberOfSeatsAndFactor(NumberOfSeatsAndFactor.TWO_SEAT);
            roomEntities.add(room);
        }
        for (int i = 0; i < hotelCreateDto.getNumberOfTripleRooms(); i++) {
            RoomEntity room = new RoomEntity();
            room.setNumberOfSeatsAndFactor(NumberOfSeatsAndFactor.THREE_SEAT);
            roomEntities.add(room);
        }
        for (int i = 0; i < hotelCreateDto.getNumberOfQuadrupleRooms(); i++) {
            RoomEntity room = new RoomEntity();
            room.setNumberOfSeatsAndFactor(NumberOfSeatsAndFactor.FOUR_SEAT);
            roomEntities.add(room);
        }
        return roomEntities;
    }

    private boolean findAvailableHotels(
            Date startDate,
            Date endDate,
            NumberOfSeatsAndFactor numberOfSeatsAndFactor,
            HotelEntity hotel) {
        List<RoomEntity> roomsWithTheRequiredNumberOfSeats = new ArrayList<>();
        for (RoomEntity room : hotel.getRooms()) {
            if (room.getNumberOfSeatsAndFactor().equals(numberOfSeatsAndFactor)) {
                roomsWithTheRequiredNumberOfSeats.add(room);
            }
        }
        if (roomsWithTheRequiredNumberOfSeats.size() == 0) {
            return false;
        }
        for (RoomEntity room : roomsWithTheRequiredNumberOfSeats) {
            if (room.getBookings().size() == 0) {
                return true;
            }
            int counterOfNonOverlappingRoomBookings = 0;
            for (BookingEntity booking : room.getBookings()) {
                if (booking.getStartDate().before(startDate)
                        && booking.getEndDate().before(startDate)
                        || booking.getStartDate().after(endDate)) {
                    counterOfNonOverlappingRoomBookings++;
                } else {
                    break;
                }
            }
            if (counterOfNonOverlappingRoomBookings == room.getBookings().size()) {
                return true;
            }
        }
        return false;
    }


}
