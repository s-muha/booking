package by.itstep.booking.service.impl;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.exeption.AppEntityNotFoundException;
import by.itstep.booking.exeption.RoomsException;
import by.itstep.booking.mapper.RoomMapper;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.service.RoomService;
import by.itstep.booking.service.config.ServiceConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    private final RoomRepository roomRepository;
    private final HotelRepository hotelRepository;
    private final RoomMapper roomMapper;
    private final ServiceConfig serviceConfig;

    @Override
    @Transactional
    public RoomFullDto findById(Integer id) {
        RoomEntity foundRoom = roomRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("RoomEntity was not found by id: " + id));
        RoomFullDto roomFullDto = roomMapper.map(foundRoom);
        log.info("Room was successfully found");
        return roomFullDto;
    }

    @Override
    @Transactional
    public List<RoomShortDto> findAll() {
        List<RoomEntity> foundRooms = roomRepository.findAll();
        List<RoomShortDto> roomShortDtos = roomMapper.map(foundRooms);
        log.info(roomShortDtos.size() + " rooms were found");
        return roomShortDtos;
    }

    @Override
    @Transactional
    public RoomFullDto create(RoomCreateDto roomCreateDto) {
        hotelRepository.findById(roomCreateDto.getHotelId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("HotelEntity was not found by id: " + roomCreateDto.getHotelId()));
        RoomEntity entityToSave = roomMapper.map(roomCreateDto);

        RoomEntity savedRoom = roomRepository.save(entityToSave);
        RoomFullDto roomFullDto = roomMapper.map(savedRoom);
        log.info("Room was successfully created");
        return roomFullDto;
    }

    @Override
    @Transactional
    public RoomFullDto update(RoomUpdateDto roomUpdateDto) {
        RoomEntity roomToUpdated = roomRepository.findById(roomUpdateDto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("RoomEntity was not found by id: " + roomUpdateDto.getId()));
        for (BookingEntity booking : roomToUpdated.getBookings()) {
            if (booking.getEndDate().after(Date.valueOf(LocalDate.now()))) {
                throw new RoomsException("Cannot update the number, it still has unfulfilled orders");
            }
        }
        roomToUpdated.setNumberOfSeatsAndFactor(roomUpdateDto.getNumberOfSeatsAndFactor());
        RoomEntity updatedRoom = roomRepository.save(roomToUpdated);
        RoomFullDto roomFullDto = roomMapper.map(updatedRoom);
        log.info("Room was successfully updated");
        return roomFullDto;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        RoomEntity roomToDeleted = roomRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("RoomEntity was not found by id: " + id));
        for (BookingEntity booking : roomToDeleted.getBookings()) {
            UserEntity user = booking.getUser();
            if (booking.getStartDate().after(Date.valueOf(LocalDate.now()))) {
                user.setAmountOfMoney(
                        user.getAmountOfMoney() + (serviceConfig.calculateCostBooking(booking.getStartDate(),
                                booking.getEndDate(), booking.getRoom(), booking.getRoom().getHotel())));
            }
            booking.setUser(user);
            booking.setDeletedAt(Instant.now());
        }
        roomToDeleted.setDeletedAt(Instant.now());
        roomRepository.save(roomToDeleted);
        log.info("Room was successfully deleted");
    }
}
