package by.itstep.booking.service.impl;

import by.itstep.booking.dto.user.*;
import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.UserRole;
import by.itstep.booking.exeption.AppEntityNotFoundException;
import by.itstep.booking.exeption.UniqueValuesIsTakenException;
import by.itstep.booking.exeption.WrongUserPasswordException;
import by.itstep.booking.mapper.UserMapper;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.service.UserService;
import by.itstep.booking.service.config.ServiceConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final ServiceConfig serviceConfig;

    @Override
    @Transactional
    public UserFullDto findById(Integer id) {
        UserEntity foundUser = userRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));
        serviceConfig.throwIfNotCurrentUserOrAdmin(foundUser.getEmail());
        UserFullDto userFullDto = userMapper.map(foundUser);
        log.info("User was successfully found");
        return userFullDto;
    }

    @Override
    @Transactional
    public List<UserShortDto> findAll() {
        List<UserEntity> foundUsers = userRepository.findAll();
        List<UserShortDto> userShortDtos = userMapper.map(foundUsers);
        System.out.println("UserServiceImpl -> " + userShortDtos.size() + " users were found");
        return userShortDtos;
    }

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto userCreateDto) {
        UserEntity entityToSave = userMapper.map(userCreateDto);
        Optional<UserEntity> entityWithSameEmail = userRepository.findByEmail(entityToSave.getEmail());
        if (entityWithSameEmail.isPresent()) {
            throw new UniqueValuesIsTakenException("Email is taken");
        }
        entityToSave.setRole(UserRole.USER);
        entityToSave.setAmountOfMoney(0.0);
        entityToSave.setPassword(passwordEncoder
                .encode(userCreateDto.getPassword() + userCreateDto.getEmail()));
        UserEntity savedUser = userRepository.save(entityToSave);
        UserFullDto userFullDto = userMapper.map(savedUser);
        log.info("User was successfully created");
        return userFullDto;
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto userUpdateDto) {
        UserEntity userToUpdated = userRepository.findById(userUpdateDto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + userUpdateDto.getId()));
        serviceConfig.throwIfNotCurrentUserOrAdmin(userToUpdated.getEmail());
        userToUpdated.setFirstName(userUpdateDto.getFirstName());
        userToUpdated.setLastName(userUpdateDto.getLastName());
        userToUpdated.setPhone(userUpdateDto.getPhone());

        UserEntity updatedUser = userRepository.save(userToUpdated);
        UserFullDto userFullDto = userMapper.map(updatedUser);
        log.info("User was successfully updated");
        return userFullDto;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        UserEntity userToDeleted = userRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));
        serviceConfig.throwIfNotCurrentUserOrAdmin(userToDeleted.getEmail());

        String emailOfAdminWhoDeleting = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<UserEntity> entityWithSameEmail = userRepository.findByEmail(emailOfAdminWhoDeleting);
        if (entityWithSameEmail.isEmpty()) {
            throw new AppEntityNotFoundException("This admin has been deleted");
        }
        for (BookingEntity booking : userToDeleted.getBookings()) {
            if (booking.getStartDate().after(Date.valueOf(LocalDate.now()))) {
                userToDeleted.setAmountOfMoney(
                        userToDeleted.getAmountOfMoney() + (serviceConfig.calculateCostBooking(booking.getStartDate(),
                                booking.getEndDate(), booking.getRoom(), booking.getRoom().getHotel())));
            }
            booking.setUser(userToDeleted);
            booking.setDeletedAt(Instant.now());
        }
        userToDeleted.setDeletedAt(Instant.now());
        userRepository.save(userToDeleted);
        log.info("User was successfully deleted");
    }

    @Override
    @Transactional
    public void changePassword(ChangePasswordDto dto) {
        UserEntity userToUpdate = userRepository.findById(dto.getUserId())
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId()));
        serviceConfig.throwIfNotCurrentUser(userToUpdate.getEmail());
        if (!passwordEncoder.matches((dto.getOldPassword() + userToUpdate.getEmail()), userToUpdate.getPassword())) {
            throw new WrongUserPasswordException("Wrong password");
        }
        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword() + userToUpdate.getEmail()));
        userRepository.save(userToUpdate);
        log.info("User password was successfully update");
    }

    @Override
    @Transactional
    public UserFullDto changeRole(ChangeRoleDto changeRoleDto) {
        UserEntity userToUpdate = userRepository.findById(changeRoleDto.getUserId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + changeRoleDto.getUserId()));
        userToUpdate.setRole(changeRoleDto.getNewRole());
        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto userFullDto = userMapper.map(updatedUser);
        log.info("User role was successfully updated");
        return userFullDto;
    }

    @Override
    @Transactional
    public void topUpAccount(Integer userId, Double amount) {
        UserEntity foundUser = userRepository.findById(userId)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + userId));
        serviceConfig.throwIfNotCurrentUserOrAdmin(foundUser.getEmail());
        foundUser.setAmountOfMoney(foundUser.getAmountOfMoney() + amount);
        UserEntity user = userRepository.save(foundUser);
        log.info("Your account has been replenished with " + amount +
                " now it is " + user.getAmountOfMoney());
    }
}
