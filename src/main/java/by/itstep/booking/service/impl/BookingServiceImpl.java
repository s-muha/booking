package by.itstep.booking.service.impl;

import by.itstep.booking.dto.booking.*;
import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.StatusBooking;
import by.itstep.booking.exeption.AppEntityNotFoundException;
import by.itstep.booking.exeption.InsufficientAmountOfMoneyInAccountException;
import by.itstep.booking.exeption.RoomsException;
import by.itstep.booking.mapper.BookingMapper;
import by.itstep.booking.repository.BookingRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.service.BookingService;
import by.itstep.booking.service.config.ServiceConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final HotelRepository hotelRepository;
    private final UserRepository userRepository;
    private final BookingMapper bookingMapper;
    private final ServiceConfig serviceConfig;


    @Override
    @Transactional
    public BookingFullDto findById(Integer id) {
        BookingEntity foundBooking = bookingRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("BookingEntity was not found by id: " + id));
        serviceConfig.throwIfNotCurrentUserOrAdmin(foundBooking.getUser().getEmail());
        BookingFullDto bookingFullDto = bookingMapper.map(foundBooking);
        log.info("User was successfully found");
        return bookingFullDto;
    }

    @Override
    @Transactional
    public List<BookingShortDto> findAll() {
        List<BookingEntity> foundBookings = bookingRepository.findAll();
        List<BookingShortDto> bookingShortDtos = bookingMapper.map(foundBookings);
        log.info(bookingShortDtos.size() + " bookings were found");
        return bookingShortDtos;
    }

    @Override
    @Transactional
    public BookingFullDto create(BookingCreateDto bookingCreateDto) {
        serviceConfig.checkBookingDates(bookingCreateDto.getStartDate(), bookingCreateDto.getEndDate());
        UserEntity foundUser = userRepository.findById(bookingCreateDto.getUserId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + bookingCreateDto.getUserId()));
        serviceConfig.throwIfNotCurrentUserOrAdmin(foundUser.getEmail());
        HotelEntity selectedHotel = hotelRepository.findById(bookingCreateDto.getHotelId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("HotelEntity was not found by id: " + bookingCreateDto.getHotelId()));

        RoomEntity roomForBooking = findFreeRoom(bookingCreateDto, selectedHotel);
        Double bookingCost = serviceConfig.calculateCostBooking(
                bookingCreateDto.getStartDate(),
                bookingCreateDto.getEndDate(),
                roomForBooking,
                selectedHotel
        );
        checkIfEnoughMoneyInAccountForCreateBooking(foundUser, bookingCost);
        foundUser.setAmountOfMoney(foundUser.getAmountOfMoney() - bookingCost);

        BookingEntity bookingToSave = bookingMapper.map(bookingCreateDto);
        bookingToSave.setUser(foundUser);
        bookingToSave.setRoom(roomForBooking);
        bookingToSave.setStatus(StatusBooking.IN_PROGRESS);

        BookingEntity savedBooking = bookingRepository.save(bookingToSave);
        BookingFullDto bookingFullDto = bookingMapper.map(savedBooking);
        log.info("Booking was successfully created");
        return bookingFullDto;
    }

    @Override
    @Transactional
    public BookingFullDto update(BookingUpdateDto bookingUpdateDto) {
        serviceConfig.checkBookingDates(bookingUpdateDto.getStartDate(), bookingUpdateDto.getEndDate());
        HotelEntity selectedHotel = hotelRepository.findById(bookingUpdateDto.getHotelId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("HotelEntity was not found by id: " + bookingUpdateDto.getHotelId()));
        BookingEntity bookingToUpdated = bookingRepository.findById(bookingUpdateDto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("BookingEntity was not found by id: " + bookingUpdateDto.getId()));
        serviceConfig.throwIfNotCurrentUserOrAdmin(bookingToUpdated.getUser().getEmail());
        RoomEntity roomToUpdatedBooking = findFreeRoom(bookingUpdateDto, selectedHotel);
        UserEntity user = bookingToUpdated.getUser();
        Double oldBookingCost = serviceConfig.calculateCostBooking(
                bookingToUpdated.getStartDate(),
                bookingToUpdated.getEndDate(),
                bookingToUpdated.getRoom(),
                bookingToUpdated.getRoom().getHotel());
        Double newBookingCost = serviceConfig.calculateCostBooking(
                bookingUpdateDto.getStartDate(),
                bookingUpdateDto.getEndDate(),
                roomToUpdatedBooking,
                selectedHotel);
        checkIfEnoughMoneyInAccountForUpdateBooking(user, oldBookingCost, newBookingCost);
        user.setAmountOfMoney(user.getAmountOfMoney() + oldBookingCost - newBookingCost);

        bookingToUpdated.setRoom(roomToUpdatedBooking);
        bookingToUpdated.setUser(user);
        bookingToUpdated.setStartDate(bookingUpdateDto.getStartDate());
        bookingToUpdated.setEndDate(bookingUpdateDto.getEndDate());
        BookingEntity updatedBooking = bookingRepository.save(bookingToUpdated);
        BookingFullDto bookingFullDto = bookingMapper.map(updatedBooking);
        log.info("Booking was successfully updated");
        return bookingFullDto;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        BookingEntity bookingToDeleted = bookingRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("BookingEntity was not found by id: " + id));
        serviceConfig.throwIfNotCurrentUserOrAdmin(bookingToDeleted.getUser().getEmail());
        UserEntity user = bookingToDeleted.getUser();
        if (bookingToDeleted.getStartDate().after(Date.valueOf(LocalDate.now()))) {
            user.setAmountOfMoney(
                    user.getAmountOfMoney() + (serviceConfig.calculateCostBooking(
                            bookingToDeleted.getStartDate(),
                            bookingToDeleted.getEndDate(),
                            bookingToDeleted.getRoom(),
                            bookingToDeleted.getRoom().getHotel())));
        }
        bookingToDeleted.setUser(user);
        bookingToDeleted.setDeletedAt(Instant.now());
        bookingRepository.save(bookingToDeleted);
        log.info("Booking was successfully deleted");
    }

    private RoomEntity findFreeRoom(BookingDto bookingDto, HotelEntity selectedHotel) {
        List<RoomEntity> roomsWithTheRequiredNumberOfSeats = new ArrayList<>();
        for (RoomEntity room : selectedHotel.getRooms()) {
            if (room.getNumberOfSeatsAndFactor().equals(bookingDto.getNumberOfSeats())) {
                roomsWithTheRequiredNumberOfSeats.add(room);
            }
        }
        if (roomsWithTheRequiredNumberOfSeats.size() == 0) {
            throw new RoomsException("This hotel does not have rooms with this number of seats");
        }
        for (RoomEntity room : roomsWithTheRequiredNumberOfSeats) {
            if (room.getBookings().size() == 0) {
                return room;
            }
            int counterOfNonOverlappingRoomBookings = 0;
            for (BookingEntity booking : room.getBookings()) {
                if (bookingDto.getEndDate().before(booking.getStartDate())
                        || bookingDto.getStartDate().after(booking.getEndDate())) {
                    counterOfNonOverlappingRoomBookings++;
                } else {
                    break;
                }
            }
            if (counterOfNonOverlappingRoomBookings == room.getBookings().size()) {
                return room;
            }
        }
        throw new RoomsException("There are no available rooms for the given dates in this hotel");
    }

    private void checkIfEnoughMoneyInAccountForCreateBooking(UserEntity user, Double bookingCost) {
        if (user.getAmountOfMoney() < bookingCost) {
            throw new InsufficientAmountOfMoneyInAccountException(
                    "You do not have enough money in your account for this booking");
        }
    }

    private void checkIfEnoughMoneyInAccountForUpdateBooking(
            UserEntity user, Double oldBookingCost, Double newBookingCost) {
        if (user.getAmountOfMoney() + oldBookingCost < newBookingCost) {
            throw new InsufficientAmountOfMoneyInAccountException(
                    "You do not have enough money in your account for this booking");
        }
    }
}
