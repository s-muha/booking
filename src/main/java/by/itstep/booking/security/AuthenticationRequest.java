package by.itstep.booking.security;

import lombok.Data;

@Data
public class AuthenticationRequest {

    private String email;
    private String password;
}
