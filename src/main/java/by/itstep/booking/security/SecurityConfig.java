package by.itstep.booking.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static by.itstep.booking.entity.enums.UserRole.ADMIN;
import static by.itstep.booking.entity.enums.UserRole.USER;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] SWAGGER_RESOURCES = {"/v2/api-docs",
            "/configuration/ui", "/swagger-resources/**",
            "/configuration/security", "/swagger-ui.html", "/webjars/**"};


    private final JwtTokenProvider jwtTokenProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(STATELESS)
                .and()
                .authorizeRequests()

                .antMatchers(HttpMethod.POST, "/authenticate").permitAll() // Метод с проверкой ответа
                .antMatchers(SWAGGER_RESOURCES).permitAll()

                .antMatchers(HttpMethod.GET, "/users").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.POST, "/users/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/users/role").hasRole(ADMIN.name())

                .antMatchers("/rooms").hasRole(ADMIN.name())
                .antMatchers("/rooms/{id}").hasRole(ADMIN.name())

                .antMatchers(HttpMethod.GET, "/hotels").hasAnyRole(ADMIN.name(), USER.name())
                .antMatchers(HttpMethod.POST, "/hotels").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.PUT, "/hotels").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.DELETE, "/hotels/**").hasRole(ADMIN.name())


                .antMatchers(HttpMethod.GET, "/countries").hasAnyRole(USER.name(), ADMIN.name())
                .antMatchers(HttpMethod.POST, "/countries").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.PUT, "/countries").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.DELETE, "/countries/**").hasRole(ADMIN.name())

                .antMatchers(HttpMethod.GET, "/bookings").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.POST, "/bookings").hasAnyRole("USER", "ADMIN")

                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
    }

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
