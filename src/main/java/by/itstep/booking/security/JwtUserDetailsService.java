package by.itstep.booking.security;

import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException
                        ("JwtUserDetailsService -> user was not found by email: " + email));
        return new JwtUserDetails(user.getEmail(), user.getPassword(), Arrays.asList(user.getRole()));
    }
}
