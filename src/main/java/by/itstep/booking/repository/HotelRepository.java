package by.itstep.booking.repository;

import by.itstep.booking.entity.HotelEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface HotelRepository extends JpaRepository<HotelEntity, Integer> {

    Optional<HotelEntity> findByNameAndCountryId(String name, Integer countryId);
}
