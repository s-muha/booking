package by.itstep.booking.entity;

import by.itstep.booking.entity.enums.StatusBooking;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Date;
import java.time.Instant;

@Data
@Entity
@Table(name = "bookings")
@Where(clause = "deleted_at IS NULL")
public class BookingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "room_id")
    private RoomEntity room;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusBooking status;

    @Column(name = "deleted_at")
    private Instant deletedAt;

}
