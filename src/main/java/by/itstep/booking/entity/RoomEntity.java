package by.itstep.booking.entity;

import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "rooms")
@Where(clause = "deleted_at IS NULL")
public class RoomEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private HotelEntity hotel;

    @Enumerated(EnumType.STRING)
    @Column(name = "number_of_seats_and_factor")
    private NumberOfSeatsAndFactor numberOfSeatsAndFactor;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<BookingEntity> bookings = new ArrayList<>();

}
