package by.itstep.booking.entity.enums;

public enum StatusBooking {

    IN_PROGRESS, COMPLETED
}
