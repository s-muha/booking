package by.itstep.booking.entity.enums;

public enum UserRole {

    USER, ADMIN
}
