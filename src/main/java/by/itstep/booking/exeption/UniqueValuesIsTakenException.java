package by.itstep.booking.exeption;

public class UniqueValuesIsTakenException extends RuntimeException {


    public UniqueValuesIsTakenException(String message){
        super(message);
    }
}
