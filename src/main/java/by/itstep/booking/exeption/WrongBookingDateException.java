package by.itstep.booking.exeption;

public class WrongBookingDateException extends RuntimeException{

    public WrongBookingDateException(String message) {
        super(message);
    }
}
