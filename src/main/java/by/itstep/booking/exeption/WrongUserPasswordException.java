package by.itstep.booking.exeption;

public class WrongUserPasswordException extends RuntimeException{

    public WrongUserPasswordException(String message) {
        super(message);
    }
}
