package by.itstep.booking.exeption;

public class InsufficientAmountOfMoneyInAccountException extends RuntimeException{

    public InsufficientAmountOfMoneyInAccountException(String message) {
        super(message);
    }
}
