package by.itstep.booking.exeption;

public class RoomsException extends RuntimeException{

    public RoomsException(String message) {
        super(message);
    }
}
