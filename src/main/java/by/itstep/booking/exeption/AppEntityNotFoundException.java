package by.itstep.booking.exeption;

public class AppEntityNotFoundException extends RuntimeException{

    public AppEntityNotFoundException(String message) {
        super(message);
    }
}
