package by.itstep.booking.controller;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import by.itstep.booking.service.HotelService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Date;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class HotelController {

    private final HotelService hotelService;

    @GetMapping("/hotels/{id}")
    public HotelFullDto findById(@PathVariable Integer id) {
        return hotelService.findById(id);
    }

    @GetMapping("/hotels")
    public List<HotelShortDto> findAll() {
        return hotelService.findAll();
    }

    @PostMapping("/hotels")
    public HotelFullDto create(@Valid @RequestBody HotelCreateDto dto) {
        return hotelService.create(dto);
    }

    @PutMapping("/hotels")
    public HotelFullDto update(@Valid @RequestBody HotelUpdateDto dto) {
        return hotelService.update(dto);
    }

    @DeleteMapping("/hotels/{id}")
    public void delete(@PathVariable Integer id) {
        hotelService.deleteById(id);
    }

    @GetMapping("/countries/{countryId}/searchAvailableHotels")
    public List<HotelShortDto> searchAvailableHotelsInCountry(
            @PathVariable Integer countryId,
            @RequestParam("numberOfSeatsAndFactor") NumberOfSeatsAndFactor numberOfSeatsAndFactor,
            @RequestParam("startDate") Date startDate,
            @RequestParam("endDate") Date endDate) {
        return hotelService.searchFreeHotelsInCountry(countryId, numberOfSeatsAndFactor, startDate, endDate);
    }
}
