package by.itstep.booking.controller;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryShortDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class CountryController {

    private final CountryService countryService;

    @GetMapping("/countries/{id}")
    public CountryFullDto findById(@PathVariable Integer id) {
        return countryService.findById(id);
    }

    @GetMapping("/countries")
    public List<CountryShortDto> findAll() {
        return countryService.findAll();
    }

    @PostMapping("/countries")
    public CountryFullDto create(@Valid @RequestBody CountryCreateDto dto) {
        return countryService.create(dto);
    }

    @PutMapping("/countries")
    public CountryFullDto update(@Valid @RequestBody CountryUpdateDto dto) {
        return countryService.update(dto);
    }

    @DeleteMapping("/countries/{id}")
    public void delete(@PathVariable Integer id) {
        countryService.deleteById(id);
    }
}
