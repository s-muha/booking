package by.itstep.booking.controller;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.service.RoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class RoomController {

    private final RoomService roomService;

    @GetMapping("/rooms/{id}")
    public RoomFullDto findById(@PathVariable Integer id) {
        return roomService.findById(id);
    }

    @GetMapping("/rooms")
    public List<RoomShortDto> findAll() {
        return roomService.findAll();
    }

    @PostMapping("/rooms")
    public RoomFullDto create(@Valid @RequestBody RoomCreateDto dto) {
        return roomService.create(dto);
    }

    @PutMapping("/rooms")
    public RoomFullDto update(@Valid @RequestBody RoomUpdateDto dto) {
        return roomService.update(dto);
    }

    @DeleteMapping("/rooms/{id}")
    public void delete(@PathVariable Integer id) {
        roomService.deleteById(id);
    }
}
