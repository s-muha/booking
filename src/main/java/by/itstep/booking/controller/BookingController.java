package by.itstep.booking.controller;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.dto.booking.BookingShortDto;
import by.itstep.booking.dto.booking.BookingUpdateDto;
import by.itstep.booking.service.BookingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class BookingController {

    private final BookingService bookingService;

    @GetMapping("/bookings/{id}")
    public BookingFullDto findById(@PathVariable Integer id) {
        return bookingService.findById(id);
    }

    @GetMapping("/bookings")
    public List<BookingShortDto> findAll() {
        return bookingService.findAll();
    }

    @PostMapping("/bookings")
    public BookingFullDto create(@Valid @RequestBody BookingCreateDto dto) {
        return bookingService.create(dto);
    }

    @PutMapping("/bookings")
    public BookingFullDto update(@Valid @RequestBody BookingUpdateDto dto) {
        return bookingService.update(dto);
    }

    @DeleteMapping("/bookings/{id}")
    public void delete(@PathVariable Integer id) {
        bookingService.deleteById(id);
    }
}
