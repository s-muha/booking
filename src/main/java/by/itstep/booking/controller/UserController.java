package by.itstep.booking.controller;

import by.itstep.booking.dto.user.*;
import by.itstep.booking.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/users/{id}")
    public UserFullDto findById(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping("/users")
    public List<UserShortDto> findAll() {
        return userService.findAll();
    }

    @PostMapping("/users")
    public UserFullDto create(@Valid @RequestBody UserCreateDto dto) {
        return userService.create(dto);
    }

    @PutMapping("/users")
    public UserFullDto update(@Valid @RequestBody UserUpdateDto dto) {
        return userService.update(dto);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Integer id) {
        userService.deleteById(id);
    }

    @PutMapping("/users/password")
    public void changePassword(@Valid @RequestBody ChangePasswordDto dto) {
        userService.changePassword(dto);
    }

    @PutMapping("/users/role")
    public UserFullDto changeRole(@Valid @RequestBody ChangeRoleDto dto) {
        return userService.changeRole(dto);
    }

    @PutMapping("/users/{userId}/topUp")
    public void topUpAccount(@PathVariable Integer userId, @RequestParam("amount") Double amount) {
        userService.topUpAccount(userId, amount);
    }
}
