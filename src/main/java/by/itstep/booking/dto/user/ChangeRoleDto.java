package by.itstep.booking.dto.user;

import by.itstep.booking.entity.enums.UserRole;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ChangeRoleDto {

    @NotBlank(message = "Id can not be blank")
    private Integer userId;

    @NotBlank(message = "New role can not be blank")
    private UserRole newRole;
}
