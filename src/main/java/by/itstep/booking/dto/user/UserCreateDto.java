package by.itstep.booking.dto.user;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserCreateDto {

    @NotBlank(message = "First name cannot be blank")
    @Size(min = 1, max = 20, message = "First name mast be between 1 and 20")
    private String firstName;

    @NotBlank(message = "Last name cannot be blank")
    @Size(min = 1, max = 20, message = "Last name mast be between 1 and 20")
    private String lastName;

    @NotBlank(message = "Phone can not be blank")
    @Size(max = 20, message = "phone number must not exceed 20 characters")
    private String phone;

    @Email(message = "Email mast have valid format")
    private String email;

    @NotBlank(message = "Password can not be null")
    @Size(min = 8, max = 100, message = "Password length must be between 8 and 100")
    private String password;

}
