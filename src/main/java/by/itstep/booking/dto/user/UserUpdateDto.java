package by.itstep.booking.dto.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserUpdateDto {

    @NotBlank(message = "Id can not be blank")
    private Integer id;

    @NotBlank(message = "First name cannot be blank")
    @Size(min = 1, max = 20, message = "First name mast be between 1 and 20")
    private String firstName;

    @NotBlank(message = "Last name cannot be blank")
    @Size(min = 1, max = 20, message = "Last name mast be between 1 and 20")
    private String lastName;

    @NotBlank(message = "Phone can not be blank")
    @Size(max = 20, message = "phone number must not exceed 20 characters")
    private String phone;

}
