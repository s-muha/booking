package by.itstep.booking.dto.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ChangePasswordDto {

    @NotBlank(message = "Id can not be blank")
    private Integer userId;

    @NotBlank(message = "Old password can not be blank")
    private String oldPassword;

    @NotBlank(message = "New Password can not be blank")
    private String newPassword;
}
