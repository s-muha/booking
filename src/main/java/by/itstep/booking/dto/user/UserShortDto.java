package by.itstep.booking.dto.user;

import by.itstep.booking.entity.enums.UserRole;
import lombok.Data;

@Data
public class UserShortDto {

    private Integer id;

    private UserRole role;

    private String firstName;

    private String lastName;

}
