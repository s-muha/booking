package by.itstep.booking.dto.user;

import by.itstep.booking.dto.booking.BookingShortDto;
import by.itstep.booking.entity.enums.UserRole;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserFullDto {

    private Integer id;

    private UserRole role;

    private String firstName;

    private String lastName;

    private String phone;

    private String email;

    private Double amountOfMoney;

    private List<BookingShortDto> bookings = new ArrayList<>();


}
