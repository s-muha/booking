package by.itstep.booking.dto.hotel;

import by.itstep.booking.dto.country.CountryShortDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.entity.enums.NumberOfStars;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HotelFullDto {

    private Integer id;

    private String name;

    private CountryShortDto country;

    private String address;

    private NumberOfStars numberOfStars;

    private Double rating;

    private Double price;

    private List<RoomShortDto> rooms = new ArrayList<>();
}
