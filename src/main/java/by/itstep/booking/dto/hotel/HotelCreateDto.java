package by.itstep.booking.dto.hotel;

import by.itstep.booking.entity.enums.NumberOfStars;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class HotelCreateDto {

    @NotBlank(message = "Name cannot be blank")
    @Size(min = 1, max = 30, message = "Name mast be between 1 and 30")
    private String name;

    @NotNull(message = "Country cannot be null")
    private Integer countryId;

    @NotBlank(message = "Address cannot be blank")
    @Size(min = 5, max = 50, message = "Address mast be between 5 and 50")
    private String address;

    @NotNull(message = "NumberOfStars cannot be null")
    private NumberOfStars numberOfStars;

    @NotBlank(message = "Price cannot be blank")
    private Double price;

    @NotBlank(message = "Number of single rooms cannot be blank")
    private Integer numberOfSingleRooms;

    @NotBlank(message = "Number of double rooms cannot be blank")
    private Integer numberOfDoubleRooms;

    @NotBlank(message = "Number of triple rooms cannot be blank")
    private Integer numberOfTripleRooms;

    @NotBlank(message = "Number of quadruple rooms cannot be blank")
    private Integer numberOfQuadrupleRooms;

}
