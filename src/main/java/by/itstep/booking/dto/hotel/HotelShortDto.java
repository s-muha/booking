package by.itstep.booking.dto.hotel;

import by.itstep.booking.dto.country.CountryShortDto;
import by.itstep.booking.entity.enums.NumberOfStars;
import lombok.Data;

@Data
public class HotelShortDto {

    private Integer id;

    private String name;

    private CountryShortDto country;

    private NumberOfStars numberOfStars;

    private Double rating;

    private Double price;

}
