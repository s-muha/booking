package by.itstep.booking.dto.hotel;

import by.itstep.booking.entity.enums.NumberOfStars;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class HotelUpdateDto {

    @NotBlank(message = "Id can not be blank")
    private Integer id;

    @NotBlank(message = "Name cannot be blank")
    @Size(min = 1, max = 30, message = "Name mast be between 1 and 30")
    private String name;

    @NotBlank(message = "Address cannot be blank")
    @Size(min = 5, max = 50, message = "Address mast be between 5 and 50")
    private String address;

    @NotNull(message = "NumberOfStars cannot be null")
    private NumberOfStars numberOfStars;

    @NotBlank(message = "Price cannot be blank")
    private Double price;

}
