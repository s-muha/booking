package by.itstep.booking.dto.room;

import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import lombok.Data;

@Data
public class RoomShortDto {

    private Integer id;

    private HotelShortDto hotel;

    private NumberOfSeatsAndFactor numberOfSeatsAndFactor;

}
