package by.itstep.booking.dto.room;

import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RoomCreateDto {

    @NotNull(message = "Hotel cannot be null")
    private Integer hotelId;

    @NotNull(message = "NumberOfSeatsAndFactor cannot be null")
    private NumberOfSeatsAndFactor numberOfSeatsAndFactor;

}
