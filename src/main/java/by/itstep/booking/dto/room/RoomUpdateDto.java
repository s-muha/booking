package by.itstep.booking.dto.room;

import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RoomUpdateDto {

    @NotBlank(message = "Id can not be blank")
    private Integer id;

    @NotNull(message = "NumberOfSeatsAndFactor cannot be null")
    private NumberOfSeatsAndFactor numberOfSeatsAndFactor;

}

