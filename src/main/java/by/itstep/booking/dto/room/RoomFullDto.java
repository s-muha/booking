package by.itstep.booking.dto.room;

import by.itstep.booking.dto.booking.BookingShortDto;
import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RoomFullDto {

    private Integer id;

    private HotelShortDto hotel;

    private NumberOfSeatsAndFactor numberOfSeatsAndFactor;

    private List<BookingShortDto> bookings = new ArrayList<>();
}
