package by.itstep.booking.dto.country;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class CountryUpdateDto {

    @NotBlank(message = "Id can not be blank")
    private Integer id;

    @NotBlank(message = "Name name cannot be blank")
    @Size(min = 2, max = 30, message = "Name mast be between 1 and 20")
    private String name;
}
