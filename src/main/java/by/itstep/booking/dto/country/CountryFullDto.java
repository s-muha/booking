package by.itstep.booking.dto.country;

import by.itstep.booking.dto.hotel.HotelShortDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CountryFullDto {

    private Integer id;

    private String name;

    private List<HotelShortDto> hotels = new ArrayList<>();


}
