package by.itstep.booking.dto.booking;

import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.user.UserShortDto;
import by.itstep.booking.entity.enums.StatusBooking;
import lombok.Data;

import java.sql.Date;

@Data
public class BookingFullDto {

    private Integer id;

    private UserShortDto user;

    private RoomShortDto room;

    private Date startDate;

    private Date endDate;

    private StatusBooking status;
}
