package by.itstep.booking.dto.booking;

import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import lombok.Data;

import java.sql.Date;

@Data
public class BookingDto {

    private NumberOfSeatsAndFactor numberOfSeats;

    private Date startDate;

    private Date endDate;

}
