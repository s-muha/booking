package by.itstep.booking.dto.booking;

import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import lombok.Data;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import java.sql.Date;

@Data
public class BookingUpdateDto extends BookingDto {

    @NotBlank(message = "Id can not be blank")
    private Integer id;

    @NotBlank(message = "HotelId cannot be blank")
    private Integer hotelId;

    @NotBlank(message = "Number of seats cannot be blank")
    private NumberOfSeatsAndFactor numberOfSeats;

    @NotBlank(message = "Start date cannot be blank")
    @FutureOrPresent(message = "Start date can't be in the past")
    private Date startDate;

    @NotBlank(message = "End date cannot be blank")
    @Future(message = "End date cannot be past or present")
    private Date endDate;
}
