package by.itstep.booking.dto.booking;

import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.entity.enums.StatusBooking;
import lombok.Data;

@Data
public class BookingShortDto {

    private Integer id;

    private RoomShortDto room;

    private StatusBooking status;

}
