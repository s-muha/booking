package by.itstep.booking.mapper;

import by.itstep.booking.dto.user.UserCreateDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.dto.user.UserShortDto;
import by.itstep.booking.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {BookingMapper.class})
public interface UserMapper {

    UserFullDto map(UserEntity userEntity);

    List<UserShortDto> map(List<UserEntity> userEntities);

    UserEntity map(UserCreateDto userCreateDto);
}
