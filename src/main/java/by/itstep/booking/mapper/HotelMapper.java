package by.itstep.booking.mapper;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.entity.HotelEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {RoomMapper.class, CountryMapper.class})
public interface HotelMapper {

    HotelFullDto map(HotelEntity hotelEntity);

    List<HotelShortDto> map(List<HotelEntity> hotelEntities);

    @Mapping(target = "country.id", source = "countryId")
    HotelEntity map(HotelCreateDto hotelCreateDto);

    HotelCreateDto mapInCreate(HotelEntity hotelEntity);
}
