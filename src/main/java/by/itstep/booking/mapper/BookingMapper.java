package by.itstep.booking.mapper;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.dto.booking.BookingShortDto;
import by.itstep.booking.entity.BookingEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class, RoomMapper.class})
public interface BookingMapper {

    BookingFullDto map(BookingEntity bookingEntity);

    List<BookingShortDto> map(List<BookingEntity> bookingEntities);

    @Mapping(target = "user.id", source = "userId")
    BookingEntity map(BookingCreateDto bookingCreateDto);
}
