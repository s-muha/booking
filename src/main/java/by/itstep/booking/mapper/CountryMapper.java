package by.itstep.booking.mapper;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryShortDto;
import by.itstep.booking.entity.CountryEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {HotelMapper.class})
public interface CountryMapper {

    CountryFullDto map(CountryEntity countryEntity);

    List<CountryShortDto> map(List<CountryEntity> countryEntities);

    CountryEntity map(CountryCreateDto countryCreateDto);
}
