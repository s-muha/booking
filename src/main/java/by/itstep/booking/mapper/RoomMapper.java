package by.itstep.booking.mapper;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.entity.RoomEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {BookingMapper.class, HotelMapper.class})
public interface RoomMapper {

    RoomFullDto map(RoomEntity roomEntity);

    List<RoomShortDto> map(List<RoomEntity> roomEntities);

    @Mapping(target = "hotel.id", source = "hotelId")
    RoomEntity map(RoomCreateDto roomCreateDto);


}
