package by.itstep.booking;

import by.itstep.booking.utils.DbCleaner;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc
public class BookingApplicationTests {

	public static final Faker FAKER = new Faker();

	@Autowired
	private DbCleaner dbCleaner;

	@BeforeEach
	public void setUp(){
		dbCleaner.clean();
	}

	@AfterEach
	public void shotDown(){
		dbCleaner.clean();
	}


	@Test
	void contextLoads() {
	}

}
