package by.itstep.booking.utils.bookingUtils;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingUpdateDto;
import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;

import java.sql.Date;
import java.time.LocalDate;

import static by.itstep.booking.BookingApplicationTests.FAKER;

public class DtoBookingHelper {

    public static BookingCreateDto generateBookingCreateDto(
            Integer userId, Integer hotelId, NumberOfSeatsAndFactor numberOfSeatsAndFactor){

        BookingCreateDto dto = new BookingCreateDto();

        dto.setUserId(userId);
        dto.setHotelId(hotelId);
        dto.setNumberOfSeats(numberOfSeatsAndFactor);
        dto.setStartDate(Date.valueOf(LocalDate.now().plusDays(FAKER.number().numberBetween(0, 60))));
        dto.setEndDate(Date.valueOf(dto.getStartDate().toLocalDate().plusDays(FAKER.number().numberBetween(2, 5))));
        return dto;
    }

    public static BookingUpdateDto generateBookingUpdateDto(
            Integer bookingId,
            Integer hotelId,
            NumberOfSeatsAndFactor numberOfSeatsAndFactor,
            Date startDate,
            Date endDate){
        BookingUpdateDto dto = new BookingUpdateDto();

       dto.setId(bookingId);
       dto.setHotelId(hotelId);
       dto.setNumberOfSeats(numberOfSeatsAndFactor);
       dto.setStartDate(startDate);
       dto.setEndDate(endDate);
        return dto;
    }
}
