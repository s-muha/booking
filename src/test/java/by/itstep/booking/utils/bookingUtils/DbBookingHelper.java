package by.itstep.booking.utils.bookingUtils;

import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.UserRole;
import by.itstep.booking.repository.BookingRepository;
import by.itstep.booking.utils.roomUtils.DbRoomHelper;
import by.itstep.booking.utils.userUtils.DbUserHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;

import static by.itstep.booking.BookingApplicationTests.FAKER;

@Component
public class DbBookingHelper {

    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private DbRoomHelper dbRoomHelper;
    @Autowired
    private DbUserHelper dbUserHelper;

    public void bookingToDb(){
        BookingEntity booking = new BookingEntity();

        booking.setUser(dbUserHelper.userToDb(UserRole.USER));
        booking.setRoom(dbRoomHelper.roomToDb());
        booking.setStartDate(Date.valueOf(LocalDate.now().plusDays(FAKER.number().numberBetween(0, 60))));
        booking.setEndDate
                (Date.valueOf(booking.getStartDate().toLocalDate().plusDays(FAKER.number().numberBetween(1, 30))));
        booking.setDeletedAt(null);
        bookingRepository.save(booking);
    }

    public BookingEntity bookingToDb(UserEntity user, RoomEntity room, Date startDate, Date endDate){
        BookingEntity booking = new BookingEntity();

        booking.setUser(user);
        booking.setRoom(room);
        booking.setStartDate(startDate);
        booking.setEndDate(endDate);
        booking.setDeletedAt(null);
        return bookingRepository.save(booking);
    }

    public BookingEntity bookingToDb(UserEntity user, RoomEntity room){
        BookingEntity booking = new BookingEntity();

        booking.setUser(user);
        booking.setRoom(room);
        booking.setStartDate(Date.valueOf(LocalDate.now().plusDays(FAKER.number().numberBetween(0, 60))));
        booking.setEndDate
                (Date.valueOf(booking.getStartDate().toLocalDate().plusDays(FAKER.number().numberBetween(1, 30))));
        booking.setDeletedAt(null);
        return bookingRepository.save(booking);
    }
}
