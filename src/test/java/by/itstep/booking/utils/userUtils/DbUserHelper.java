package by.itstep.booking.utils.userUtils;

import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.UserRole;
import by.itstep.booking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import static by.itstep.booking.BookingApplicationTests.FAKER;

@Component
public class DbUserHelper {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserEntity userToDb(UserRole role) {
        UserEntity user = new UserEntity();

        user.setRole(role);
        user.setFirstName(FAKER.name().firstName());
        user.setLastName(FAKER.name().lastName());
        user.setPhone(FAKER.phoneNumber().cellPhone());
        user.setEmail(FAKER.internet().emailAddress());
        user.setPassword(passwordEncoder.encode((FAKER.random().nextInt(10000000, 99999999)) +
                user.getEmail()));
        user.setAmountOfMoney(0.0);
        user.setDeletedAt(null);
        return userRepository.save(user);
    }

    public UserEntity userToDb(UserRole role, Double amountOfMoney) {
        UserEntity user = new UserEntity();

        user.setRole(role);
        user.setFirstName(FAKER.name().firstName());
        user.setLastName(FAKER.name().lastName());
        user.setPhone(FAKER.phoneNumber().cellPhone());
        user.setEmail(FAKER.internet().emailAddress());
        user.setPassword(passwordEncoder.encode((FAKER.random().nextInt(10000000, 99999999)) +
                user.getEmail()));
        user.setAmountOfMoney(amountOfMoney);
        user.setDeletedAt(null);
        return userRepository.save(user);
    }

    public UserEntity userToDb(UserEntity user) {
        user.setPassword(passwordEncoder.encode(user.getPassword() + user.getEmail()));
        return userRepository.save(user);
    }
    public void deleteUserFromDb(UserEntity user) {
        userRepository.deleteById(user.getId());
    }

}

