package by.itstep.booking.utils.userUtils;

import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.UserRole;

import static by.itstep.booking.BookingApplicationTests.FAKER;

public class EntityUserHelper {

    public static UserEntity prepareUserEntity(UserRole role) {
        UserEntity user = new UserEntity();

        user.setRole(role);
        user.setFirstName(FAKER.name().firstName());
        user.setLastName(FAKER.name().lastName());
        user.setPhone(FAKER.phoneNumber().cellPhone());
        user.setEmail(FAKER.internet().emailAddress());
        user.setPassword(String.valueOf(FAKER.random().nextInt(10000000, 99999999)));
        user.setAmountOfMoney(0.0);
        user.setDeletedAt(null);

        return user;
    }

    public static UserEntity prepareUserEntityForChangePassword(UserRole role) {
        UserEntity user = new UserEntity();

        user.setRole(role);
        user.setFirstName(FAKER.name().firstName());
        user.setLastName(FAKER.name().lastName());
        user.setPhone(FAKER.phoneNumber().cellPhone());
        user.setEmail(FAKER.internet().emailAddress());
        user.setAmountOfMoney(0.0);
        user.setDeletedAt(null);

        return user;
    }
}
