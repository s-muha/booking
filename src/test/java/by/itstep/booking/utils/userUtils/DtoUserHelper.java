package by.itstep.booking.utils.userUtils;


import by.itstep.booking.dto.user.ChangePasswordDto;
import by.itstep.booking.dto.user.ChangeRoleDto;
import by.itstep.booking.dto.user.UserCreateDto;
import by.itstep.booking.dto.user.UserUpdateDto;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.UserRole;

import static by.itstep.booking.BookingApplicationTests.FAKER;



public class DtoUserHelper {

    public static UserCreateDto generateUserCreateDto(){
        UserCreateDto userCreateDto = new UserCreateDto();
        userCreateDto.setFirstName(FAKER.name().firstName());
        userCreateDto.setLastName(FAKER.name().lastName());
        userCreateDto.setPhone(FAKER.phoneNumber().cellPhone());
        userCreateDto.setEmail(FAKER.internet().emailAddress());
        userCreateDto.setPassword(String.valueOf(FAKER.random().nextInt(10000000, 99999999)));
        return userCreateDto;
    }

    public static UserUpdateDto generateUserUpdateDto(UserEntity entity){
        UserUpdateDto userUpdateDto = new UserUpdateDto();
        userUpdateDto.setId(entity.getId());
        userUpdateDto.setFirstName(FAKER.name().firstName());
        userUpdateDto.setLastName(FAKER.name().lastName());
        userUpdateDto.setPhone(FAKER.phoneNumber().cellPhone());
        return userUpdateDto;
    }

    public static ChangePasswordDto generateChangePasswordDto(Integer userId, String oldPassword, String newPassword){
        ChangePasswordDto changePasswordDto = new ChangePasswordDto();
        changePasswordDto.setUserId(userId);
        changePasswordDto.setOldPassword(oldPassword);
        changePasswordDto.setNewPassword(newPassword);
        return changePasswordDto;
    }

    public static ChangeRoleDto generateChangeRoleDto(Integer userId, UserRole role){
        ChangeRoleDto changeRoleDto = new ChangeRoleDto();

        changeRoleDto.setUserId(userId);
        changeRoleDto.setNewRole(role);
        return changeRoleDto;
    }

}
