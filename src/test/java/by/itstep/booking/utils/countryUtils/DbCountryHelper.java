package by.itstep.booking.utils.countryUtils;

import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static by.itstep.booking.BookingApplicationTests.FAKER;

@Component
public class DbCountryHelper {

    @Autowired
    private CountryRepository countryRepository;


    public CountryEntity countryToDb(){
        CountryEntity country = new CountryEntity();

        country.setName(FAKER.address().country());
        country.setDeletedAt(null);
        return countryRepository.save(country);
    }
}
