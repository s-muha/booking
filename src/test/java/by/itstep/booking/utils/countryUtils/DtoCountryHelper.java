package by.itstep.booking.utils.countryUtils;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.entity.CountryEntity;

import static by.itstep.booking.BookingApplicationTests.FAKER;


public class DtoCountryHelper {

    public static CountryCreateDto generateCountryCreateDto(){
        CountryCreateDto countryCreateDto = new CountryCreateDto();
        countryCreateDto.setName(FAKER.address().country());
        return countryCreateDto;
    }

    public static CountryUpdateDto generateCountryUpdateDto(CountryEntity country){
        CountryUpdateDto countryUpdateDto = new CountryUpdateDto();
        countryUpdateDto.setId(country.getId());
        countryUpdateDto.setName(FAKER.address().country());
        return countryUpdateDto;
    }
}
