package by.itstep.booking.utils.hotelUtils;

import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.enums.NumberOfStars;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.utils.countryUtils.DbCountryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static by.itstep.booking.BookingApplicationTests.FAKER;

@Component
public class DbHotelHelper {

    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private DbCountryHelper dbCountryHelper;

    public HotelEntity hotelToDb() {

        CountryEntity country = dbCountryHelper.countryToDb();
        HotelEntity hotel = new HotelEntity();

        hotel.setName(FAKER.company().name());
        hotel.setCountry(country);
        hotel.setAddress(FAKER.address().fullAddress());
        hotel.setNumberOfStars(FAKER.options().option(
                NumberOfStars.ONE_STAR,
                NumberOfStars.TWO_STARS,
                NumberOfStars.THREE_STARS,
                NumberOfStars.THREE_STARS_PLUS,
                NumberOfStars.FOUR_STARS,
                NumberOfStars.FOUR_STARS_PLUS,
                NumberOfStars.FIVE_STARS,
                NumberOfStars.FIVE_STARS_PLUS
        ));
        hotel.setRating((double) FAKER.random().nextInt(0, 10));
        hotel.setPrice((double) FAKER.random().nextInt(10, 1000));
        hotel.setDeletedAt(null);

        return hotelRepository.save(hotel);
    }

    public HotelEntity hotelToDb(CountryEntity country) {
        HotelEntity hotel = new HotelEntity();

        hotel.setName(FAKER.company().name());
        hotel.setCountry(country);
        hotel.setAddress(FAKER.address().fullAddress());
        hotel.setNumberOfStars(FAKER.options().option(
                NumberOfStars.ONE_STAR,
                NumberOfStars.TWO_STARS,
                NumberOfStars.THREE_STARS,
                NumberOfStars.THREE_STARS_PLUS,
                NumberOfStars.FOUR_STARS,
                NumberOfStars.FOUR_STARS_PLUS,
                NumberOfStars.FIVE_STARS,
                NumberOfStars.FIVE_STARS_PLUS
        ));
        hotel.setRating((double) FAKER.random().nextInt(0, 10));
        hotel.setPrice((double) FAKER.random().nextInt(10, 1000));
        hotel.setDeletedAt(null);

        return hotelRepository.save(hotel);
    }

}
