package by.itstep.booking.utils.hotelUtils;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.enums.NumberOfStars;

import static by.itstep.booking.BookingApplicationTests.FAKER;

public class DtoHotelHelper {

    public static HotelCreateDto generateHotelCreateDto(Integer countryId){

        HotelCreateDto dto = new HotelCreateDto();

        dto.setName(FAKER.country().name());
        dto.setCountryId(countryId);
        dto.setAddress(FAKER.address().fullAddress());
        dto.setPrice((double) FAKER.random().nextInt(10, 1000));
        dto.setNumberOfStars((FAKER.options().option(
                NumberOfStars.ONE_STAR,
                NumberOfStars.TWO_STARS,
                NumberOfStars.THREE_STARS,
                NumberOfStars.THREE_STARS_PLUS,
                NumberOfStars.FOUR_STARS,
                NumberOfStars.FOUR_STARS_PLUS,
                NumberOfStars.FIVE_STARS,
                NumberOfStars.FIVE_STARS_PLUS
        )));
        dto.setNumberOfSingleRooms(FAKER.random().nextInt(0, 100));
        dto.setNumberOfDoubleRooms(FAKER.random().nextInt(0, 100));
        dto.setNumberOfTripleRooms(FAKER.random().nextInt(0, 100));
        dto.setNumberOfQuadrupleRooms(FAKER.random().nextInt(0, 100));
        return dto;
    }

    public static HotelUpdateDto generateHotelUpdateDto(HotelEntity entity){

        HotelUpdateDto dto = new HotelUpdateDto();

        dto.setId(entity.getId());
        dto.setName(FAKER.country().name());
        dto.setAddress(FAKER.address().fullAddress());
        dto.setPrice((double) FAKER.random().nextInt(10, 1000));
        dto.setNumberOfStars((FAKER.options().option(
                NumberOfStars.ONE_STAR,
                NumberOfStars.TWO_STARS,
                NumberOfStars.THREE_STARS,
                NumberOfStars.THREE_STARS_PLUS,
                NumberOfStars.FOUR_STARS,
                NumberOfStars.FOUR_STARS_PLUS,
                NumberOfStars.FIVE_STARS,
                NumberOfStars.FIVE_STARS_PLUS
        )));
        return dto;
    }
}
