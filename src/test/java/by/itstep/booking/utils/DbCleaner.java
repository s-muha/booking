package by.itstep.booking.utils;


import by.itstep.booking.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DbCleaner {

    @Autowired
    UserRepository userRepository;
    @Autowired
    BookingRepository bookingRepository;
    @Autowired
    RoomRepository roomRepository;
    @Autowired
    HotelRepository hotelRepository;
    @Autowired
    CountryRepository countryRepository;

    public void clean(){
        bookingRepository.deleteAllInBatch();
        userRepository.deleteAllInBatch();
        roomRepository.deleteAllInBatch();
        hotelRepository.deleteAllInBatch();
        countryRepository.deleteAllInBatch();
    }
}
