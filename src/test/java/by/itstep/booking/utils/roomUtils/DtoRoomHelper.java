package by.itstep.booking.utils.roomUtils;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;

import static by.itstep.booking.BookingApplicationTests.FAKER;


public class DtoRoomHelper {

    public static RoomCreateDto generateRoomCreateDto(Integer hotelId){
        RoomCreateDto dto = new RoomCreateDto();
        dto.setHotelId(hotelId);
        dto.setNumberOfSeatsAndFactor(FAKER.options().option(
                NumberOfSeatsAndFactor.ONE_SEAT,
                NumberOfSeatsAndFactor.TWO_SEAT,
                NumberOfSeatsAndFactor.THREE_SEAT,
                NumberOfSeatsAndFactor.FOUR_SEAT));
        return dto;
    }

    public static RoomUpdateDto generateRoomUpdateDto(RoomEntity entity){
        RoomUpdateDto dto = new RoomUpdateDto();

        dto.setId(entity.getId());
        dto.setNumberOfSeatsAndFactor(FAKER.options().option(
                NumberOfSeatsAndFactor.ONE_SEAT,
                NumberOfSeatsAndFactor.TWO_SEAT,
                NumberOfSeatsAndFactor.THREE_SEAT,
                NumberOfSeatsAndFactor.FOUR_SEAT));
        return dto;
    }
}
