package by.itstep.booking.utils.roomUtils;

import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.utils.countryUtils.DbCountryHelper;
import by.itstep.booking.utils.hotelUtils.DbHotelHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static by.itstep.booking.BookingApplicationTests.FAKER;

@Component
public class DbRoomHelper {

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private DbCountryHelper dbCountryHelper;
    @Autowired
    private DbHotelHelper dbHotelHelper;

    public RoomEntity roomToDb() {

        CountryEntity country = dbCountryHelper.countryToDb();
        HotelEntity hotel = dbHotelHelper.hotelToDb(country);

        RoomEntity room = new RoomEntity();
        room.setHotel(hotel);
        room.setNumberOfSeatsAndFactor(FAKER.options().option(
                NumberOfSeatsAndFactor.ONE_SEAT,
                NumberOfSeatsAndFactor.TWO_SEAT,
                NumberOfSeatsAndFactor.THREE_SEAT,
                NumberOfSeatsAndFactor.FOUR_SEAT));
        room.setDeletedAt(null);
        return roomRepository.save(room);
    }

    public RoomEntity roomToDb(HotelEntity hotel, NumberOfSeatsAndFactor numberOfSeatsAndFactor) {

        RoomEntity room = new RoomEntity();
        room.setHotel(hotel);
        room.setNumberOfSeatsAndFactor(numberOfSeatsAndFactor);
        room.setDeletedAt(null);

        return roomRepository.save(room);
    }
}
