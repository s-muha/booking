package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.UserRole;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.utils.JwtHelper;
import by.itstep.booking.utils.countryUtils.DbCountryHelper;
import by.itstep.booking.utils.userUtils.DbUserHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static by.itstep.booking.utils.countryUtils.DtoCountryHelper.generateCountryCreateDto;
import static by.itstep.booking.utils.countryUtils.DtoCountryHelper.generateCountryUpdateDto;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CountryControllerTest extends BookingApplicationTests {

    @Autowired
    private DbCountryHelper dbCountryHelper;
    @Autowired
    private DbUserHelper dbUserHelper;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CountryRepository countryRepository;


    @Test
    public void findById_happyPath() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenExistingUser = jwtHelper.createToken(existingUser.getEmail());

        CountryEntity existingCountry = dbCountryHelper.countryToDb();
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(GET, "/countries/" + existingCountry.getId())
                        .header("Authorization", tokenExistingUser))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        CountryFullDto foundCountry = objectMapper.readValue(bytes, CountryFullDto.class);
        //then
        Assertions.assertEquals(existingCountry.getId(), foundCountry.getId());
    }

    @Test
    public void findById_whenNotInDb() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenExistingUser = jwtHelper.createToken(existingUser.getEmail());

        CountryEntity existingCountry = dbCountryHelper.countryToDb();
        int nonExistentId = existingCountry.getId() + 1;
        //when
        mockMvc.perform(request(GET, "/countries/" + nonExistentId)
                        .header("Authorization", tokenExistingUser))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void findById_whenNotAuthenticatedUserOrNotAdmin() throws Exception {
        //given
        CountryEntity existingCountry = dbCountryHelper.countryToDb();
        //when
        mockMvc.perform(request(GET, "/countries/" + existingCountry.getId()))
                .andExpect(status().isForbidden());
    }

    @Test
    public void findAll_happyPath() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenExistingUser = jwtHelper.createToken(existingUser.getEmail());

        dbCountryHelper.countryToDb();
        dbCountryHelper.countryToDb();
        dbCountryHelper.countryToDb();
        //when
        mockMvc.perform(request(GET, "/countries")
                        .header("Authorization", tokenExistingUser))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    public void findAll_whenNotAuthenticatedUserOrNotAdmin() throws Exception {
        //given
        dbCountryHelper.countryToDb();
        dbCountryHelper.countryToDb();
        dbCountryHelper.countryToDb();
        //when
        mockMvc.perform(request(GET, "/countries"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void create_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryCreateDto createDto = generateCountryCreateDto();
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/countries")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        CountryFullDto foundCountry = objectMapper.readValue(bytes, CountryFullDto.class);
        //then
        Assertions.assertNotNull(foundCountry.getId());
        Assertions.assertEquals(foundCountry.getName(), createDto.getName());
    }

    @Test
    public void create_whenNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        CountryCreateDto createDto = generateCountryCreateDto();
        //when
        mockMvc.perform(request(POST, "/countries")
                        .header("Authorization", tokenUserNotAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void create_whenNameIsTaken() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryEntity existingCountry = dbCountryHelper.countryToDb();
        String nameIsTaken = existingCountry.getName();

        CountryCreateDto createDto = generateCountryCreateDto();
        createDto.setName(nameIsTaken);
        //when
        mockMvc.perform(request(POST, "/countries")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void update_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryEntity countryToUpdate = dbCountryHelper.countryToDb();

        CountryUpdateDto updateDto = generateCountryUpdateDto(countryToUpdate);
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(PUT, "/countries")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        CountryFullDto foundCountry = objectMapper.readValue(bytes, CountryFullDto.class);
        //then
        Assertions.assertEquals(countryToUpdate.getId(), foundCountry.getId());
        Assertions.assertEquals(updateDto.getName(), foundCountry.getName());
    }

    @Test
    public void update_whenNotAdmin() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryEntity countryToUpdate = dbCountryHelper.countryToDb();

        CountryUpdateDto updateDto = generateCountryUpdateDto(countryToUpdate);
        //when
        mockMvc.perform(request(PUT, "/countries")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void update_whenNameIsTaken() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryEntity country = dbCountryHelper.countryToDb();
        String nameIsTaken = country.getName();

        CountryEntity countryToUpdate = dbCountryHelper.countryToDb();

        CountryUpdateDto updateDto = generateCountryUpdateDto(countryToUpdate);
        updateDto.setName(nameIsTaken);
        //when
        mockMvc.perform(request(POST, "/countries")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());
        //then
    }

    @Test
    public void update_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryEntity countryToUpdate = dbCountryHelper.countryToDb();
        int nonExistentId = countryToUpdate.getId() + 1;

        CountryUpdateDto updateDto = generateCountryUpdateDto(countryToUpdate);
        updateDto.setId(nonExistentId);
        //when
        mockMvc.perform(request(PUT, "/countries")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void delete_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryEntity countryToDelete = dbCountryHelper.countryToDb();
        //when
        mockMvc.perform(request(DELETE, "/countries/" + countryToDelete.getId())
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json"))
                .andExpect(status().isOk());
        //then
        mockMvc.perform(request(GET, "/countries/" + countryToDelete.getId())
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_whenNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        CountryEntity countryToDelete = dbCountryHelper.countryToDb();
        //when
        mockMvc.perform(request(DELETE, "/countries/" + countryToDelete.getId())
                        .header("Authorization", tokenUserNotAdmin)
                        .header("Content-Type", "application/json"))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void delete_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryEntity countryToDelete = dbCountryHelper.countryToDb();
        int nonExistentId = countryToDelete.getId() + 1;

        //when
        mockMvc.perform(request(DELETE, "/countries/" + nonExistentId)
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json"))
                .andExpect(status().isNotFound());
        //then
    }


}
