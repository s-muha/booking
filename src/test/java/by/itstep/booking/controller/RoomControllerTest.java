package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.UserRole;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.utils.JwtHelper;
import by.itstep.booking.utils.countryUtils.DbCountryHelper;
import by.itstep.booking.utils.hotelUtils.DbHotelHelper;
import by.itstep.booking.utils.roomUtils.DbRoomHelper;
import by.itstep.booking.utils.userUtils.DbUserHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static by.itstep.booking.utils.roomUtils.DtoRoomHelper.generateRoomCreateDto;
import static by.itstep.booking.utils.roomUtils.DtoRoomHelper.generateRoomUpdateDto;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RoomControllerTest extends BookingApplicationTests {

    @Autowired
    private DbUserHelper dbUserHelper;
    @Autowired
    private DbRoomHelper dbRoomHelper;
    @Autowired
    private DbCountryHelper dbCountryHelper;
    @Autowired
    private DbHotelHelper dbHotelHelper;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private RoomRepository roomRepository;

    @Test
    public void findById_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(userAdmin.getEmail());

        RoomEntity existingRoom = dbRoomHelper.roomToDb();
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(GET, "/rooms/" + existingRoom.getId())
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        RoomFullDto foundRoom = objectMapper.readValue(bytes, RoomFullDto.class);
        //then
        Assertions.assertEquals(existingRoom.getId(), foundRoom.getId());
    }

    @Test
    public void findById_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(userAdmin.getEmail());

        RoomEntity existingRoom = dbRoomHelper.roomToDb();
        int nonExistentId = existingRoom.getId() + 1;
        //when
        mockMvc.perform(request(GET, "/rooms/" + nonExistentId)
                        .header("Authorization", token))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void findById_whenNotAuthenticatedAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        RoomEntity existingRoom = dbRoomHelper.roomToDb();
        //when
        mockMvc.perform(request(GET, "/rooms/" + existingRoom.getId())
                        .header("Authorization", tokenUserNotAdmin))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void findAll_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(userAdmin.getEmail());

        dbRoomHelper.roomToDb();
        dbRoomHelper.roomToDb();
        dbRoomHelper.roomToDb();
        //when
        mockMvc.perform(request(GET, "/rooms")
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
        //then
    }

    @Test
    public void findAll_whenNotAuthenticatedAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        dbRoomHelper.roomToDb();
        dbRoomHelper.roomToDb();
        dbRoomHelper.roomToDb();
        //when
        mockMvc.perform(request(GET, "/rooms")
                        .header("Authorization", tokenUserNotAdmin))
                .andExpect(status().isForbidden());
        //then

    }

    @Test
    public void create_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomCreateDto createDto = generateRoomCreateDto(hotel.getId());
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/rooms")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        RoomFullDto foundRoom = objectMapper.readValue(bytes, RoomFullDto.class);
        //then
        Assertions.assertNotNull(foundRoom.getId());
        Assertions.assertEquals(createDto.getHotelId(), foundRoom.getHotel().getId());
    }

    @Test
    public void create_whenNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomCreateDto createDto = generateRoomCreateDto(hotel.getId());
        //when
        mockMvc.perform(request(POST, "/rooms")
                        .header("Authorization", tokenUserNotAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void create_whenNotHotelInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        HotelEntity hotelForCreateHotel = dbHotelHelper.hotelToDb();
        int nonExistentHotelId = hotelForCreateHotel.getId() + 1;

        RoomCreateDto createDto = generateRoomCreateDto(nonExistentHotelId);
        //when
        mockMvc.perform(request(POST, "/rooms")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void update_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        RoomEntity roomToUpdate = dbRoomHelper.roomToDb();

        RoomUpdateDto updateDto = generateRoomUpdateDto(roomToUpdate);
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(PUT, "/rooms")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        RoomFullDto foundRoom = objectMapper.readValue(bytes, RoomFullDto.class);
        //then
        Assertions.assertEquals(roomToUpdate.getId(), foundRoom.getId());
        Assertions.assertEquals(foundRoom.getNumberOfSeatsAndFactor(), updateDto.getNumberOfSeatsAndFactor());
    }

    @Test
    public void update_whenNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        RoomEntity roomToUpdate = dbRoomHelper.roomToDb();

        RoomUpdateDto updateDto = generateRoomUpdateDto(roomToUpdate);
        //when
        mockMvc.perform(request(PUT, "/rooms")
                        .header("Authorization", tokenUserNotAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void update_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        RoomEntity roomToUpdate = dbRoomHelper.roomToDb();
        int nonExistentId = roomToUpdate.getId() + 1;

        RoomUpdateDto updateDto = generateRoomUpdateDto(roomToUpdate);
        updateDto.setId(nonExistentId);
        //when
        mockMvc.perform(request(PUT, "/rooms")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void delete_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        RoomEntity roomToDelete = dbRoomHelper.roomToDb();
        //when
        mockMvc.perform(request(DELETE, "/rooms/" + roomToDelete.getId())
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isOk());
        //then
        mockMvc.perform(request(GET, "/rooms/" + roomToDelete.getId())
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_whenNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        RoomEntity roomToDelete = dbRoomHelper.roomToDb();
        //when
        mockMvc.perform(request(DELETE, "/rooms/" + roomToDelete.getId())
                        .header("Authorization", tokenUserNotAdmin))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void delete_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        RoomEntity roomToDelete = dbRoomHelper.roomToDb();
        int nonExistentId = roomToDelete.getId() + 1;
        //when
        mockMvc.perform(request(DELETE, "/rooms/" + nonExistentId)
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isNotFound());
        //then
    }


}
