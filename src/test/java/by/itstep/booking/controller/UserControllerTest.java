package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.user.*;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.UserRole;
import by.itstep.booking.utils.JwtHelper;
import by.itstep.booking.utils.userUtils.DbUserHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static by.itstep.booking.utils.userUtils.DtoUserHelper.*;
import static by.itstep.booking.utils.userUtils.EntityUserHelper.prepareUserEntity;
import static by.itstep.booking.utils.userUtils.EntityUserHelper.prepareUserEntityForChangePassword;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class UserControllerTest extends BookingApplicationTests {

    @Autowired
    private DbUserHelper dbUserHelper;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void findById_whenAuthenticatedAdmin_happyPath() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);

        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(GET, "/users/" + existingUser.getId())
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);

        //then
        Assertions.assertEquals(existingUser.getId(), foundUser.getId());
    }

    @Test
    public void findById_whenCurrentUserAuthenticated_happyPath() throws Exception {
        //given
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER);

        String token = jwtHelper.createToken(currentUser.getEmail());
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(GET, "/users/" + currentUser.getId())
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);

        //then
        Assertions.assertEquals(currentUser.getId(), foundUser.getId());
    }

    @Test
    public void findById_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        int nonExistentId = existingUser.getId() + 1;

        String token = jwtHelper.createToken(userAdmin.getEmail());
        //when
        mockMvc.perform(request(GET, "/users/" + nonExistentId)
                        .header("Authorization", token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findById_whenNotAuthenticatedCurrentUserOrAdmin() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);

        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(GET, "/users/" + existingUser.getId())
                        .header("Authorization", token))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void findAll_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(userAdmin.getEmail());

        dbUserHelper.userToDb(UserRole.USER);
        dbUserHelper.userToDb(UserRole.USER);
        dbUserHelper.userToDb(UserRole.USER);
        //when
        mockMvc.perform(request(GET, "/users")
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)));
    }

    @Test
    public void findAll_whenNotAdmin() throws Exception {
        //given
        UserEntity existingUser1 = dbUserHelper.userToDb(UserRole.USER);
        UserEntity existingUser2 = dbUserHelper.userToDb(UserRole.USER);
        UserEntity existingUser3 = dbUserHelper.userToDb(UserRole.USER);
        UserEntity existingUser4 = dbUserHelper.userToDb(UserRole.USER);

        String token = jwtHelper.createToken(existingUser1.getEmail());
        //when
        mockMvc.perform(request(GET, "/users")
                        .header("Authorization", token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void create_happyPath() throws Exception {
        //given
        UserCreateDto createDto = generateUserCreateDto();
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/users")
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);
        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(UserRole.USER, foundUser.getRole());
    }

    @Test
    public void create_whenEmailIsTaken() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        String emailIsTaken = existingUser.getEmail();

        UserCreateDto createDto = generateUserCreateDto();
        createDto.setEmail(emailIsTaken);
        //when
        mockMvc.perform(request(POST, "/users")
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void update_whenCurrentUser_happyPath() throws Exception {
        //given
        UserEntity userToUpdate = dbUserHelper.userToDb(UserRole.USER);
        String token = jwtHelper.createToken(userToUpdate.getEmail());

        UserUpdateDto userUpdateDto = generateUserUpdateDto(userToUpdate);

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(PUT, "/users")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(userUpdateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);

        //then
        Assertions.assertEquals(userToUpdate.getId(), foundUser.getId());
        Assertions.assertEquals(userUpdateDto.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(userUpdateDto.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(userUpdateDto.getPhone(), foundUser.getPhone());
    }

    @Test
    public void update_whenAdmin_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(userAdmin.getEmail());

        UserEntity userToUpdate = dbUserHelper.userToDb(UserRole.USER);

        UserUpdateDto userUpdateDto = generateUserUpdateDto(userToUpdate);
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(PUT, "/users")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(userUpdateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);
        //then
        Assertions.assertEquals(userToUpdate.getId(), foundUser.getId());
        Assertions.assertEquals(userUpdateDto.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(userUpdateDto.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(userUpdateDto.getPhone(), foundUser.getPhone());
    }

    @Test
    public void update_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        UserEntity userToUpdate = dbUserHelper.userToDb(UserRole.USER);

        Integer nonExistentId = userToUpdate.getId() + 1;
        UserUpdateDto userUpdateDto = generateUserUpdateDto(userToUpdate);
        userUpdateDto.setId(nonExistentId);
        //when
        mockMvc.perform(request(PUT, "/users")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(userUpdateDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void update_whenNotCurrentUser() throws Exception {
        //given
        UserEntity notCurrentUser = dbUserHelper.userToDb(UserRole.USER);
        String token = jwtHelper.createToken(notCurrentUser.getEmail());

        UserEntity userToUpdate = dbUserHelper.userToDb(UserRole.USER);
        UserUpdateDto userUpdateDto = generateUserUpdateDto(userToUpdate);
        //when
        mockMvc.perform(request(PUT, "/users")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(userUpdateDto)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void delete_whenCurrentUser_happyPath() throws Exception {
        //given
        UserEntity userToDelete = dbUserHelper.userToDb(UserRole.USER);
        String token = jwtHelper.createToken(userToDelete.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/users/" + userToDelete.getId())
                        .header("Authorization", token)
                        .header("Content-Type", "application/json"))
                .andExpect(status().isOk());
        //then
        mockMvc.perform(request(GET, "/users/" + userToDelete.getId())
                        .header("Authorization", token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_whenAdmin_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        UserEntity userToDelete = dbUserHelper.userToDb(UserRole.USER);
        //when
        mockMvc.perform(request(DELETE, "/users/" + userToDelete.getId())
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json"))
                .andExpect(status().isOk());

        mockMvc.perform(request(GET, "/users/" + userToDelete.getId())
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void delete_whenNotCurrentUserOrAdmin() throws Exception {
        //given
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());

        UserEntity userToDelete = dbUserHelper.userToDb(UserRole.USER);
        //when
        mockMvc.perform(request(DELETE, "/users/" + userToDelete.getId())
                        .header("Authorization", token)
                        .header("Content-Type", "application/json"))
                .andExpect(status().isUnauthorized());
        //then
    }

    @Test
    public void delete_whenAdminWasDeletedAfterAuthorization() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(userAdmin.getEmail());
        dbUserHelper.deleteUserFromDb(userAdmin);

        UserEntity userToDelete = dbUserHelper.userToDb(UserRole.USER);
        //when
        mockMvc.perform(request(DELETE, "/users/" + userToDelete.getId())
                        .header("Authorization", token)
                        .header("Content-Type", "application/json"))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void delete_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(userAdmin.getEmail());

        UserEntity userToDelete = dbUserHelper.userToDb(UserRole.USER);
        int nonExistentId = userToDelete.getId() + 1;
        //when
        mockMvc.perform(request(DELETE, "/users/" + nonExistentId)
                        .header("Authorization", token)
                        .header("Content-Type", "application/json"))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void changePassword_happyPath() throws Exception {
        //given
        UserEntity user = prepareUserEntityForChangePassword(UserRole.USER);
        String oldPassword = "12345678";
        user.setPassword(oldPassword);

        UserEntity userWithOldPasswordFromDb = dbUserHelper.userToDb(user);
        String token = jwtHelper.createToken(userWithOldPasswordFromDb.getEmail());

        String newPassword = "qwertyui";
        ChangePasswordDto changePasswordDto = generateChangePasswordDto(userWithOldPasswordFromDb.getId(),
                oldPassword, newPassword);
        //when
        mockMvc.perform(request(PUT, "/users/password")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(changePasswordDto)))
                .andExpect(status().isOk());
        //then
    }

    @Test
    public void changePassword_whenNotCurrentUser() throws Exception {
        //given
        UserEntity user = prepareUserEntityForChangePassword(UserRole.USER);
        String oldPassword = "12345678";
        user.setPassword(oldPassword);

        UserEntity userWithOldPasswordFromDb = dbUserHelper.userToDb(user);

        UserEntity notCurrentUser = dbUserHelper.userToDb(UserRole.USER);
        String token = jwtHelper.createToken(notCurrentUser.getEmail());

        String newPassword = "qwertyui";
        ChangePasswordDto changePasswordDto = generateChangePasswordDto(userWithOldPasswordFromDb.getId(),
                oldPassword, newPassword);
        //when
        mockMvc.perform(request(PUT, "/users/password")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(changePasswordDto)))
                .andExpect(status().isUnauthorized());
        //then
    }

    @Test
    public void changePassword_whenCurrentUserWithRoleAdmin() throws Exception {
        //given
        UserEntity user = prepareUserEntityForChangePassword(UserRole.USER);
        String oldPassword = "12345678";
        user.setPassword(oldPassword);

        UserEntity userWithOldPasswordFromDb = dbUserHelper.userToDb(user);

        UserEntity currentUserWithRoleAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUserWithRoleAdmin.getEmail());

        String newPassword = "qwertyui";
        ChangePasswordDto changePasswordDto = generateChangePasswordDto(userWithOldPasswordFromDb.getId(),
                oldPassword, newPassword);
        //when
        mockMvc.perform(request(PUT, "/users/password")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(changePasswordDto)))
                .andExpect(status().isUnauthorized());
        //then
    }

    @Test
    public void changePassword_whenNotInDb() throws Exception {
        //given
        UserEntity user = prepareUserEntityForChangePassword(UserRole.USER);
        String oldPassword = "12345678";
        user.setPassword(oldPassword);

        UserEntity userWithOldPasswordFromDb = dbUserHelper.userToDb(user);
        String token = jwtHelper.createToken(userWithOldPasswordFromDb.getEmail());

        String newPassword = "qwertyui";
        Integer nonExistentId = userWithOldPasswordFromDb.getId() + 1;
        ChangePasswordDto changePasswordDto = generateChangePasswordDto(nonExistentId, oldPassword, newPassword);
        //when
        mockMvc.perform(request(PUT, "/users/password")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(changePasswordDto)))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void changePassword_whenWrongOldPassword() throws Exception {
        //given
        UserEntity user = prepareUserEntityForChangePassword(UserRole.USER);
        String oldPassword = "12345678";
        user.setPassword(oldPassword);

        UserEntity userWithOldPasswordFromDb = dbUserHelper.userToDb(user);
        String token = jwtHelper.createToken(userWithOldPasswordFromDb.getEmail());

        String newPassword = "qwertyui";
        String wrongOldPassword = "87654321";
        ChangePasswordDto changePasswordDto = generateChangePasswordDto(userWithOldPasswordFromDb.getId(),
                wrongOldPassword, newPassword);
        //when
        mockMvc.perform(request(PUT, "/users/password")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(changePasswordDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void changeRole_happyPath() throws Exception {
        //given
        UserEntity userToChangeRole = dbUserHelper.userToDb(UserRole.USER);

        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenAdmin = jwtHelper.createToken(userAdmin.getEmail());

        UserRole newRole = UserRole.ADMIN;
        ChangeRoleDto changeRoleDto = generateChangeRoleDto(userToChangeRole.getId(), newRole);

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(PUT, "/users/role")
                        .header("Authorization", tokenAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(changeRoleDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);
        //then
        Assertions.assertEquals(foundUser.getId(), userToChangeRole.getId());
        Assertions.assertEquals(foundUser.getRole(), newRole);
    }

    @Test
    public void changeRole_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenAdmin = jwtHelper.createToken(userAdmin.getEmail());

        UserEntity userToChangeRole = dbUserHelper.userToDb(UserRole.USER);

        UserRole newRole = UserRole.ADMIN;
        Integer nonExistentId = userToChangeRole.getId() + 1;
        ChangeRoleDto changeRoleDto = generateChangeRoleDto(nonExistentId, newRole);

        //when
        mockMvc.perform(request(PUT, "/users/role")
                        .header("Authorization", tokenAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(changeRoleDto)))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void changeRole_whenNotAdmin() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenAdmin = jwtHelper.createToken(userAdmin.getEmail());

        UserEntity userToChangeRole = dbUserHelper.userToDb(UserRole.USER);

        UserRole newRole = UserRole.ADMIN;
        ChangeRoleDto changeRoleDto = generateChangeRoleDto(userToChangeRole.getId(), newRole);

        //when
        mockMvc.perform(request(PUT, "/users/role")
                        .header("Authorization", tokenAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(changeRoleDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void topUpAccount_happyPath() throws Exception {
        //given
        UserEntity user = prepareUserEntity(UserRole.USER);
        user.setAmountOfMoney(1000.0);

        UserEntity userToTopUpAccount = dbUserHelper.userToDb(user);
        Double userAmount = userToTopUpAccount.getAmountOfMoney();
        Double amountToTopUpAccount = 10000.0;

        String tokenUserToTopUpAccount = jwtHelper.createToken(userToTopUpAccount.getEmail());
        //when
        mockMvc.perform(request(PUT, "/users/" + userToTopUpAccount.getId() + "/topUp")
                        .header("Content-Type", "application/json")
                        .header("Authorization", tokenUserToTopUpAccount)
                        .param("amount", String.valueOf(amountToTopUpAccount)))
                .andExpect(status().isOk());
        MvcResult mvcResult = mockMvc
                .perform(request(GET, "/users/" + userToTopUpAccount.getId())
                        .header("Authorization", tokenUserToTopUpAccount))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);
        //then
        Assertions.assertEquals(foundUser.getId(), userToTopUpAccount.getId());
        Assertions.assertEquals(foundUser.getAmountOfMoney(), amountToTopUpAccount + userAmount);
    }

    @Test
    public void topUpAccount_whenCurrentUserWithRoleAdmin() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        UserEntity user = prepareUserEntity(UserRole.USER);
        user.setAmountOfMoney(1000.0);

        UserEntity userToTopUpAccount = dbUserHelper.userToDb(user);
        Double userAmount = userToTopUpAccount.getAmountOfMoney();
        Double amountToTopUpAccount = 10000.0;
        //when
        mockMvc.perform(request(PUT, "/users/" + userToTopUpAccount.getId() + "/topUp")
                        .header("Content-Type", "application/json")
                        .header("Authorization", tokenUserAdmin)
                        .param("amount", String.valueOf(amountToTopUpAccount)))
                .andExpect(status().isOk());
        MvcResult mvcResult = mockMvc
                .perform(request(GET, "/users/" + userToTopUpAccount.getId())
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);
        //then
        Assertions.assertEquals(foundUser.getId(), userToTopUpAccount.getId());
        Assertions.assertEquals(foundUser.getAmountOfMoney(), amountToTopUpAccount + userAmount);
    }

    @Test
    public void topUpAccount_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        UserEntity user = prepareUserEntity(UserRole.USER);
        user.setAmountOfMoney(1000.0);

        UserEntity userToTopUpAccount = dbUserHelper.userToDb(user);
        Double userAmount = userToTopUpAccount.getAmountOfMoney();
        Double amountToTopUpAccount = 10000.0;
        int nonExistentId = userToTopUpAccount.getId() + 1;
        //when
        mockMvc.perform(request(PUT, "/users/" + nonExistentId + "/topUp")
                        .header("Content-Type", "application/json")
                        .header("Authorization", tokenUserAdmin)
                        .param("amount", String.valueOf(amountToTopUpAccount)))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void topUpAccount_whenNotCurrentUserOrAdmin() throws Exception {
        //given
        UserEntity notCurrentUserOrAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserAdmin = jwtHelper.createToken(notCurrentUserOrAdmin.getEmail());

        UserEntity user = prepareUserEntity(UserRole.USER);
        user.setAmountOfMoney(1000.0);

        UserEntity userToTopUpAccount = dbUserHelper.userToDb(user);
        Double userAmount = userToTopUpAccount.getAmountOfMoney();
        Double amountToTopUpAccount = 10000.0;
        //when
        mockMvc.perform(request(PUT, "/users/" + userToTopUpAccount.getId() + "/topUp")
                        .header("Content-Type", "application/json")
                        .header("Authorization", tokenUserAdmin)
                        .param("amount", String.valueOf(amountToTopUpAccount)))
                .andExpect(status().isUnauthorized());
        //then
    }


}
