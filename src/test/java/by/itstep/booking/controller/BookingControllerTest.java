package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.dto.booking.BookingUpdateDto;
import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import by.itstep.booking.entity.enums.UserRole;
import by.itstep.booking.exeption.AppEntityNotFoundException;
import by.itstep.booking.repository.BookingRepository;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.utils.JwtHelper;
import by.itstep.booking.utils.bookingUtils.DbBookingHelper;
import by.itstep.booking.utils.hotelUtils.DbHotelHelper;
import by.itstep.booking.utils.roomUtils.DbRoomHelper;
import by.itstep.booking.utils.userUtils.DbUserHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.sql.Date;
import java.time.LocalDate;

import static by.itstep.booking.utils.bookingUtils.DtoBookingHelper.generateBookingCreateDto;
import static by.itstep.booking.utils.bookingUtils.DtoBookingHelper.generateBookingUpdateDto;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BookingControllerTest extends BookingApplicationTests {

    @Autowired
    private DbUserHelper dbUserHelper;
    @Autowired
    private DbHotelHelper dbHotelHelper;
    @Autowired
    private DbBookingHelper dbBookingHelper;
    @Autowired
    private DbRoomHelper dbRoomHelper;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private UserRepository userRepository;


    @Test
    public void findById_whenAuthenticatedAdmin_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        RoomEntity existingRoom = dbRoomHelper.roomToDb();

        BookingEntity existingBooking = dbBookingHelper.bookingToDb(existingUser, existingRoom);
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(GET, "/bookings/" + existingBooking.getId())
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        //then
        Assertions.assertEquals(existingBooking.getId(), foundBooking.getId());
    }

    @Test
    public void findById_whenAuthenticatedCurrentUser_happyPath() throws Exception {
        //given
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        RoomEntity existingRoom = dbRoomHelper.roomToDb();

        BookingEntity existingBooking = dbBookingHelper.bookingToDb(currentUser, existingRoom);
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(GET, "/bookings/" + existingBooking.getId())
                        .header("Authorization", tokenCurrentUser))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        //then
        Assertions.assertEquals(existingBooking.getId(), foundBooking.getId());
    }

    @Test
    public void findById_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        RoomEntity existingRoom = dbRoomHelper.roomToDb();

        BookingEntity existingBooking = dbBookingHelper.bookingToDb(existingUser, existingRoom);
        int nonExistentId = existingBooking.getId() + 1;
        //when
        mockMvc.perform(request(GET, "/bookings/" + nonExistentId)
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void findById_whenNotAuthenticatedCurrentUserOrAdmin() throws Exception {
        //given
        UserEntity notCurrentUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenNotCurrentUser = jwtHelper.createToken(notCurrentUser.getEmail());

        UserEntity userForBooking = dbUserHelper.userToDb(UserRole.USER);
        RoomEntity roomForBooking = dbRoomHelper.roomToDb();

        BookingEntity existingBooking = dbBookingHelper.bookingToDb(userForBooking, roomForBooking);
        //when
        mockMvc.perform(request(GET, "/bookings/" + existingBooking.getId())
                        .header("Authorization", tokenNotCurrentUser))
                .andExpect(status().isUnauthorized());
        //then
    }

    @Test
    public void findAll_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        dbBookingHelper.bookingToDb();
        dbBookingHelper.bookingToDb();
        dbBookingHelper.bookingToDb();

        //when
        mockMvc.perform(request(GET, "/bookings")
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
        //then
        Assertions.assertEquals(3, bookingRepository.findAll().size());
    }

    @Test
    public void findAll_whenNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        dbBookingHelper.bookingToDb();
        dbBookingHelper.bookingToDb();
        dbBookingHelper.bookingToDb();

        //when
        mockMvc.perform(request(GET, "/bookings")
                        .header("Authorization", tokenUserNotAdmin))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void create_happyPath() throws Exception {
        //given
        Double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);

        BookingCreateDto createDto =
                generateBookingCreateDto(currentUser.getId(), hotel.getId(), currentRoom.getNumberOfSeatsAndFactor());
        createDto.setStartDate(Date.valueOf(LocalDate.now().plusDays(1)));
        createDto.setEndDate(Date.valueOf(LocalDate.now().plusDays(2)));
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        UserEntity foundUser = userRepository.findById(currentUser.getId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + currentUser.getId()));

        Double costBooking = 2 * hotel.getPrice() * createDto.getNumberOfSeats().getCostFactor();
        //then
        Assertions.assertNotNull(foundBooking.getId());
        Assertions.assertEquals(foundUser.getAmountOfMoney(), amountOfMoneyOnAccountCurrentUser - costBooking);
    }

    @Test
    public void create_whenNotCurrentUserOrNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        Double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);

        BookingCreateDto createDto =
                generateBookingCreateDto(currentUser.getId(), hotel.getId(), currentRoom.getNumberOfSeatsAndFactor());
        createDto.setStartDate(Date.valueOf(LocalDate.now().plusDays(1)));
        createDto.setEndDate(Date.valueOf(LocalDate.now().plusDays(2)));
        //when
        mockMvc
                .perform(request(POST, "/bookings")
                        .header("Authorization", tokenUserNotAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isUnauthorized());
        //then
    }

    @Test
    public void create_whenStartDateEarlierThanNow() throws Exception {
        //given
        Double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);

        BookingCreateDto createDto =
                generateBookingCreateDto(currentUser.getId(), hotel.getId(), currentRoom.getNumberOfSeatsAndFactor());
        createDto.setStartDate(Date.valueOf(LocalDate.now().minusDays(1)));
        createDto.setEndDate(Date.valueOf(LocalDate.now().plusDays(2)));
        //when
        mockMvc.perform(request(POST, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void create_whenStartDateOfTheBookingCannotBeLaterThanTheEndDate() throws Exception {
        //given
        Double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);

        BookingCreateDto createDto =
                generateBookingCreateDto(currentUser.getId(), hotel.getId(), currentRoom.getNumberOfSeatsAndFactor());
        createDto.setStartDate(Date.valueOf(LocalDate.now().plusDays(10)));
        createDto.setEndDate(Date.valueOf(LocalDate.now().plusDays(5)));
        //when
        mockMvc.perform(request(POST, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void create_whenNotUserInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        Double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        int nonExistentUserId = currentUser.getId() + 1;

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);

        BookingCreateDto createDto =
                generateBookingCreateDto(nonExistentUserId, hotel.getId(), currentRoom.getNumberOfSeatsAndFactor());
        createDto.setStartDate(Date.valueOf(LocalDate.now().plusDays(1)));
        createDto.setEndDate(Date.valueOf(LocalDate.now().plusDays(2)));
        //when
        mockMvc.perform(request(POST, "/bookings")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void create_whenNotHotelInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        Double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);
        int nonExistentHotelId = currentUser.getId() + 1;

        BookingCreateDto createDto =
                generateBookingCreateDto(
                        currentUser.getId(),
                        nonExistentHotelId,
                        currentRoom.getNumberOfSeatsAndFactor());
        createDto.setStartDate(Date.valueOf(LocalDate.now().plusDays(1)));
        createDto.setEndDate(Date.valueOf(LocalDate.now().plusDays(2)));
        //when
        mockMvc.perform(request(POST, "/bookings")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void create_whenThereIsNotEnoughMoney() throws Exception {
        //given
        Double amountOfMoneyOnAccountCurrentUser = 0.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);

        BookingCreateDto createDto =
                generateBookingCreateDto(currentUser.getId(), hotel.getId(), currentRoom.getNumberOfSeatsAndFactor());
        createDto.setStartDate(Date.valueOf(LocalDate.now().plusDays(1)));
        createDto.setEndDate(Date.valueOf(LocalDate.now().plusDays(2)));
        //when
        mockMvc.perform(request(POST, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isNotAcceptable());
        //then
    }

    @Test
    public void create_whenTheHotelDoesNotHaveRoomsWithTheRequiredNumberOfBeds() throws Exception {
        //given
        Double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);

        BookingCreateDto createDto =
                generateBookingCreateDto(currentUser.getId(), hotel.getId(), NumberOfSeatsAndFactor.ONE_SEAT);
        createDto.setStartDate(Date.valueOf(LocalDate.now().plusDays(1)));
        createDto.setEndDate(Date.valueOf(LocalDate.now().plusDays(2)));
        //when
        mockMvc.perform(request(POST, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void create_whenThereAreNoAvailableRoomsWithTheRequiredNumberOfSeats() throws Exception {
        //given
        Double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity reservedRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);

        UserEntity userWhichReservedRoom = dbUserHelper.userToDb(UserRole.USER);
        dbBookingHelper.bookingToDb(
                userWhichReservedRoom,
                reservedRoom,
                Date.valueOf(LocalDate.now().plusDays(1)),
                Date.valueOf(LocalDate.now().plusDays(2)));

        BookingCreateDto createDto =
                generateBookingCreateDto(currentUser.getId(), hotel.getId(), NumberOfSeatsAndFactor.FOUR_SEAT);
        createDto.setStartDate(Date.valueOf(LocalDate.now().plusDays(1)));
        createDto.setEndDate(Date.valueOf(LocalDate.now().plusDays(2)));
        //when
        mockMvc.perform(request(POST, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void update_happyPath() throws Exception {
        //given
        double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);
        RoomEntity roomToUpdate = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.ONE_SEAT);

        Date startDate = Date.valueOf(LocalDate.now().plusDays(1));
        Date endDate = Date.valueOf(LocalDate.now().plusDays(2));

        BookingEntity bookingToUpdate = dbBookingHelper.bookingToDb(currentUser, currentRoom, startDate, endDate);

        Date startDateToUpdate = Date.valueOf(LocalDate.now().plusDays(5));
        Date endDateToUpdate = Date.valueOf(LocalDate.now().plusDays(10));

        BookingUpdateDto updateDto =
                generateBookingUpdateDto(
                        bookingToUpdate.getId(),
                        hotel.getId(),
                        NumberOfSeatsAndFactor.ONE_SEAT,
                        startDateToUpdate, endDateToUpdate);
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(PUT, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        UserEntity foundUser = userRepository.findById(currentUser.getId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + currentUser.getId()));

        double costBooking = 6 * hotel.getPrice() * NumberOfSeatsAndFactor.ONE_SEAT.getCostFactor();

        double amountToBeReturned =
                2 * hotel.getPrice() * bookingToUpdate.getRoom().getNumberOfSeatsAndFactor().getCostFactor();

        //then
        Assertions.assertNotNull(foundBooking.getId());
        Assertions.assertEquals(foundUser.getAmountOfMoney(),
                amountOfMoneyOnAccountCurrentUser + amountToBeReturned - costBooking);
        Assertions.assertEquals(foundBooking.getRoom().getId(), roomToUpdate.getId());
    }

    @Test
    public void update_whenStartDateEarlierThanNow() throws Exception {
        //given
        double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);
        dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.ONE_SEAT);

        Date startDate = Date.valueOf(LocalDate.now().plusDays(1));
        Date endDate = Date.valueOf(LocalDate.now().plusDays(2));

        BookingEntity bookingToUpdate = dbBookingHelper.bookingToDb(currentUser, currentRoom, startDate, endDate);

        Date startDateToUpdate = Date.valueOf(LocalDate.now().minusDays(5));
        Date endDateToUpdate = Date.valueOf(LocalDate.now().plusDays(10));

        BookingUpdateDto updateDto =
                generateBookingUpdateDto(
                        bookingToUpdate.getId(),
                        hotel.getId(),
                        NumberOfSeatsAndFactor.ONE_SEAT,
                        startDateToUpdate, endDateToUpdate);
        //when
        mockMvc.perform(request(PUT, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void update_whenStartDateOfTheBookingCannotBeLaterThanTheEndDate() throws Exception {
        //given
        double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);
        dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.ONE_SEAT);

        Date startDate = Date.valueOf(LocalDate.now().plusDays(1));
        Date endDate = Date.valueOf(LocalDate.now().plusDays(2));

        BookingEntity bookingToUpdate = dbBookingHelper.bookingToDb(currentUser, currentRoom, startDate, endDate);

        Date startDateToUpdate = Date.valueOf(LocalDate.now().plusDays(5));
        Date endDateToUpdate = Date.valueOf(LocalDate.now().plusDays(1));

        BookingUpdateDto updateDto =
                generateBookingUpdateDto(
                        bookingToUpdate.getId(),
                        hotel.getId(),
                        NumberOfSeatsAndFactor.ONE_SEAT,
                        startDateToUpdate, endDateToUpdate);
        //when
        mockMvc.perform(request(PUT, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void update_whenNotBookingInDb() throws Exception {
        //given
        double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);
        dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.ONE_SEAT);

        Date startDate = Date.valueOf(LocalDate.now().plusDays(1));
        Date endDate = Date.valueOf(LocalDate.now().plusDays(2));

        BookingEntity bookingToUpdate = dbBookingHelper.bookingToDb(currentUser, currentRoom, startDate, endDate);
        int nonExistentBookingId = bookingToUpdate.getId() + 1;

        Date startDateToUpdate = Date.valueOf(LocalDate.now().plusDays(5));
        Date endDateToUpdate = Date.valueOf(LocalDate.now().plusDays(10));

        BookingUpdateDto updateDto =
                generateBookingUpdateDto(
                        nonExistentBookingId,
                        hotel.getId(),
                        NumberOfSeatsAndFactor.ONE_SEAT,
                        startDateToUpdate, endDateToUpdate);
        //when
        mockMvc.perform(request(PUT, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void update_whenNotCurrentUserOrNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);
        dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.ONE_SEAT);

        Date startDate = Date.valueOf(LocalDate.now().plusDays(1));
        Date endDate = Date.valueOf(LocalDate.now().plusDays(2));

        BookingEntity bookingToUpdate = dbBookingHelper.bookingToDb(currentUser, currentRoom, startDate, endDate);

        Date startDateToUpdate = Date.valueOf(LocalDate.now().plusDays(5));
        Date endDateToUpdate = Date.valueOf(LocalDate.now().plusDays(10));

        BookingUpdateDto updateDto =
                generateBookingUpdateDto(
                        bookingToUpdate.getId(),
                        hotel.getId(),
                        NumberOfSeatsAndFactor.ONE_SEAT,
                        startDateToUpdate, endDateToUpdate);
        //when
        mockMvc.perform(request(PUT, "/bookings")
                        .header("Authorization", tokenUserNotAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isUnauthorized());
        //then
    }

    @Test
    public void update_whenTheHotelDoesNotHaveRoomsWithTheRequiredNumberOfBeds() throws Exception {
        //given
        double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);
        dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);

        Date startDate = Date.valueOf(LocalDate.now().plusDays(1));
        Date endDate = Date.valueOf(LocalDate.now().plusDays(2));

        BookingEntity bookingToUpdate = dbBookingHelper.bookingToDb(currentUser, currentRoom, startDate, endDate);

        Date startDateToUpdate = Date.valueOf(LocalDate.now().plusDays(5));
        Date endDateToUpdate = Date.valueOf(LocalDate.now().plusDays(10));

        BookingUpdateDto updateDto =
                generateBookingUpdateDto(
                        bookingToUpdate.getId(),
                        hotel.getId(),
                        NumberOfSeatsAndFactor.ONE_SEAT,
                        startDateToUpdate, endDateToUpdate);
        //when
        mockMvc.perform(request(PUT, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void update_whenThereAreNoAvailableRoomsWithTheRequiredNumberOfSeats() throws Exception {
        //given
        double amountOfMoneyOnAccountCurrentUser = 10000.0;
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.FOUR_SEAT);
        RoomEntity reservedRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.ONE_SEAT);

        Date startDate = Date.valueOf(LocalDate.now().plusDays(1));
        Date endDate = Date.valueOf(LocalDate.now().plusDays(2));

        BookingEntity bookingToUpdate = dbBookingHelper.bookingToDb(currentUser, currentRoom, startDate, endDate);

        Date startDateToUpdate = Date.valueOf(LocalDate.now().plusDays(5));
        Date endDateToUpdate = Date.valueOf(LocalDate.now().plusDays(10));

        dbBookingHelper.bookingToDb(currentUser, reservedRoom, startDateToUpdate, endDateToUpdate);

        BookingUpdateDto updateDto =
                generateBookingUpdateDto(
                        bookingToUpdate.getId(),
                        hotel.getId(),
                        NumberOfSeatsAndFactor.ONE_SEAT,
                        startDateToUpdate, endDateToUpdate);
        //when
        mockMvc.perform(request(PUT, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void update_whenThereIsNotEnoughMoney() throws Exception {
        //given
        HotelEntity hotel = dbHotelHelper.hotelToDb();
        RoomEntity currentRoom = dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.ONE_SEAT);
        dbRoomHelper.roomToDb(hotel, NumberOfSeatsAndFactor.ONE_SEAT);

        double amountOfMoneyOnAccountCurrentUser = 0;

        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER, amountOfMoneyOnAccountCurrentUser);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        Date startDate = Date.valueOf(LocalDate.now().plusDays(1));
        Date endDate = Date.valueOf(LocalDate.now().plusDays(2));

        BookingEntity bookingToUpdate = dbBookingHelper.bookingToDb(currentUser, currentRoom, startDate, endDate);

        Date startDateToUpdate = Date.valueOf(LocalDate.now().plusDays(1));
        Date endDateToUpdate = Date.valueOf(LocalDate.now().plusDays(3));

        BookingUpdateDto updateDto =
                generateBookingUpdateDto(
                        bookingToUpdate.getId(),
                        hotel.getId(),
                        NumberOfSeatsAndFactor.ONE_SEAT,
                        startDateToUpdate, endDateToUpdate);
        //when
        mockMvc.perform(request(PUT, "/bookings")
                        .header("Authorization", tokenCurrentUser)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotAcceptable());
        //then
    }

    @Test
    public void delete_happyPath() throws Exception {
        //given
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        RoomEntity existingRoom = dbRoomHelper.roomToDb();

        BookingEntity bookingToDelete = dbBookingHelper.bookingToDb(currentUser, existingRoom);

        //when
        mockMvc.perform(request(DELETE, "/bookings/" + bookingToDelete.getId())
                        .header("Authorization", tokenCurrentUser))
                .andExpect(status().isOk());
        //then
        mockMvc.perform(request(GET, "/bookings/" + bookingToDelete.getId())
                        .header("Authorization", tokenCurrentUser))
                .andExpect(status().isNotFound());

        UserEntity foundUser = userRepository.findById(currentUser.getId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + currentUser.getId()));

        Assertions.assertNotEquals(0.0, foundUser.getAmountOfMoney());
    }

    @Test
    public void delete_whenNotCurrentUserOrNotAdmin() throws Exception {
        //given
        UserEntity notCurrentUserOrNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenNotCurrentUserOrNotAdmin = jwtHelper.createToken(notCurrentUserOrNotAdmin.getEmail());

        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER);
        RoomEntity existingRoom = dbRoomHelper.roomToDb();

        BookingEntity bookingToDelete = dbBookingHelper.bookingToDb(currentUser, existingRoom);

        //when
        mockMvc.perform(request(DELETE, "/bookings/" + bookingToDelete.getId())
                        .header("Authorization", tokenNotCurrentUserOrNotAdmin))
                .andExpect(status().isUnauthorized());
        //then

    }

    @Test
    public void delete_whenNotInDb() throws Exception {
        //given
        UserEntity currentUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenCurrentUser = jwtHelper.createToken(currentUser.getEmail());

        RoomEntity existingRoom = dbRoomHelper.roomToDb();

        BookingEntity bookingToDelete = dbBookingHelper.bookingToDb(currentUser, existingRoom);
        int nonExistentBookingId = bookingToDelete.getId() + 1;

        //when
        mockMvc.perform(request(DELETE, "/bookings/" + nonExistentBookingId)
                        .header("Authorization", tokenCurrentUser))
                .andExpect(status().isNotFound());
        //then
    }
}
