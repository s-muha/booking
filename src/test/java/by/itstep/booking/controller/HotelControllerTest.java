package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.entity.enums.NumberOfSeatsAndFactor;
import by.itstep.booking.entity.enums.UserRole;
import by.itstep.booking.utils.JwtHelper;
import by.itstep.booking.utils.bookingUtils.DbBookingHelper;
import by.itstep.booking.utils.countryUtils.DbCountryHelper;
import by.itstep.booking.utils.hotelUtils.DbHotelHelper;
import by.itstep.booking.utils.roomUtils.DbRoomHelper;
import by.itstep.booking.utils.userUtils.DbUserHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import static by.itstep.booking.utils.hotelUtils.DtoHotelHelper.generateHotelCreateDto;
import static by.itstep.booking.utils.hotelUtils.DtoHotelHelper.generateHotelUpdateDto;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HotelControllerTest extends BookingApplicationTests {

    @Autowired
    private DbHotelHelper dbHotelHelper;
    @Autowired
    private DbUserHelper dbUserHelper;
    @Autowired
    private DbCountryHelper dbCountryHelper;
    @Autowired
    private DbRoomHelper dbRoomHelper;
    @Autowired
    private DbBookingHelper dbBookingHelper;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void findById_happyPath() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenExistingUser = jwtHelper.createToken(existingUser.getEmail());

        HotelEntity existingHotel = dbHotelHelper.hotelToDb();
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(GET, "/hotels/" + existingHotel.getId())
                        .header("Authorization", tokenExistingUser))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        HotelFullDto foundHotel = objectMapper.readValue(bytes, HotelFullDto.class);
        //then
        Assertions.assertEquals(existingHotel.getId(), foundHotel.getId());
    }

    @Test
    public void findById_whenNotInDb() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenExistingUser = jwtHelper.createToken(existingUser.getEmail());

        HotelEntity existingHotel = dbHotelHelper.hotelToDb();
        int nonExistentId = existingHotel.getId() + 1;
        //when
        mockMvc.perform(request(GET, "/hotels/" + nonExistentId)
                        .header("Authorization", tokenExistingUser))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void findById_whenNotAuthenticatedUserOrAdmin() throws Exception {
        //given
        HotelEntity existingHotel = dbHotelHelper.hotelToDb();
        //when
        mockMvc.perform(request(GET, "/hotels/" + existingHotel.getId()))
                .andExpect(status().isForbidden());
    }

    @Test
    public void findAll_happyPath() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenExistingUser = jwtHelper.createToken(existingUser.getEmail());

        dbHotelHelper.hotelToDb();
        dbHotelHelper.hotelToDb();
        dbHotelHelper.hotelToDb();
        //when
        mockMvc.perform(request(GET, "/hotels")
                        .header("Authorization", tokenExistingUser))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
        //then
    }

    @Test
    public void findAll_whenNotAuthenticatedUserOrNotAdmin() throws Exception {
        //given
        dbHotelHelper.hotelToDb();
        dbHotelHelper.hotelToDb();
        dbHotelHelper.hotelToDb();
        //when
        mockMvc.perform(request(GET, "/hotels"))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void create_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryEntity countryForCreateHotel = dbCountryHelper.countryToDb();

        HotelCreateDto createDto = generateHotelCreateDto(countryForCreateHotel.getId());
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/hotels")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        HotelFullDto foundHotel = objectMapper.readValue(bytes, HotelFullDto.class);
        int numberOfRoomsInHotel = createDto.getNumberOfSingleRooms() + createDto.getNumberOfDoubleRooms() +
                createDto.getNumberOfTripleRooms() + createDto.getNumberOfQuadrupleRooms();
        //then
        Assertions.assertNotNull(foundHotel.getId());
        Assertions.assertEquals(foundHotel.getRooms().size(), numberOfRoomsInHotel);
    }

    @Test
    public void create_whenNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        CountryEntity countryForCreateHotel = dbCountryHelper.countryToDb();

        HotelCreateDto createDto = generateHotelCreateDto(countryForCreateHotel.getId());
        //when
        mockMvc.perform(request(POST, "/hotels")
                        .header("Authorization", tokenUserNotAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void create_whenNameIsTakenInCountry() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        HotelEntity existingHotel = dbHotelHelper.hotelToDb();
        String nameIsTaken = existingHotel.getName();

        HotelCreateDto createDto = generateHotelCreateDto(existingHotel.getCountry().getId());
        createDto.setName(nameIsTaken);
        //when
        mockMvc.perform(request(POST, "/hotels")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
        //then
    }

    @Test
    public void create_whenNotCountryInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryEntity countryForCreateHotel = dbCountryHelper.countryToDb();
        int nonExistentCountryId = countryForCreateHotel.getId() + 1;

        HotelCreateDto createDto = generateHotelCreateDto(countryForCreateHotel.getId());
        createDto.setCountryId(nonExistentCountryId);
        //when
        mockMvc.perform(request(POST, "/hotels")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void update_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        HotelEntity hotelToUpdate = dbHotelHelper.hotelToDb();

        HotelUpdateDto updateDto = generateHotelUpdateDto(hotelToUpdate);
        //when
        MvcResult mvcResult = mockMvc
                .perform(request(PUT, "/hotels")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        HotelFullDto foundHotel = objectMapper.readValue(bytes, HotelFullDto.class);
        //then
        Assertions.assertEquals(hotelToUpdate.getId(), foundHotel.getId());
        Assertions.assertEquals(foundHotel.getName(), updateDto.getName());
    }

    @Test
    public void update_whenNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        HotelEntity hotelToUpdate = dbHotelHelper.hotelToDb();

        HotelUpdateDto updateDto = generateHotelUpdateDto(hotelToUpdate);
        //when
        mockMvc.perform(request(PUT, "/hotels")
                        .header("Authorization", tokenUserNotAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void update_whenNameIsTakenInCountry() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        CountryEntity existingCountry = dbCountryHelper.countryToDb();

        HotelEntity existingHotel = dbHotelHelper.hotelToDb(existingCountry);
        String nameIsTaken = existingHotel.getName();

        HotelEntity hotelToUpdate = dbHotelHelper.hotelToDb(existingCountry);

        HotelUpdateDto updateDto = generateHotelUpdateDto(hotelToUpdate);
        updateDto.setName(nameIsTaken);
        //when
        mockMvc.perform(request(PUT, "/hotels")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());
        //then
    }

    @Test
    public void update_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        HotelEntity hotelToUpdate = dbHotelHelper.hotelToDb();
        int nonExistentId = hotelToUpdate.getId() + 1;

        HotelUpdateDto updateDto = generateHotelUpdateDto(hotelToUpdate);
        updateDto.setId(nonExistentId);
        //when
        mockMvc.perform(request(PUT, "/hotels")
                        .header("Authorization", tokenUserAdmin)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    public void delete_happyPath() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        HotelEntity hotelToDelete = dbHotelHelper.hotelToDb();
        //when
        mockMvc.perform(request(DELETE, "/hotels/" + hotelToDelete.getId())
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isOk());
        //then
        mockMvc.perform(request(GET, "/hotels/" + hotelToDelete.getId())
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_whenNotAdmin() throws Exception {
        //given
        UserEntity userNotAdmin = dbUserHelper.userToDb(UserRole.USER);
        String tokenUserNotAdmin = jwtHelper.createToken(userNotAdmin.getEmail());

        HotelEntity hotelToDelete = dbHotelHelper.hotelToDb();
        //when
        mockMvc.perform(request(DELETE, "/hotels/" + hotelToDelete.getId())
                        .header("Authorization", tokenUserNotAdmin)
                        .header("Content-Type", "application/json"))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void delete_whenNotInDb() throws Exception {
        //given
        UserEntity userAdmin = dbUserHelper.userToDb(UserRole.ADMIN);
        String tokenUserAdmin = jwtHelper.createToken(userAdmin.getEmail());

        HotelEntity hotelToDelete = dbHotelHelper.hotelToDb();
        int nonExistentId = hotelToDelete.getId() + 1;
        //when
        mockMvc.perform(request(DELETE, "/hotels/" + nonExistentId)
                        .header("Authorization", tokenUserAdmin))
                .andExpect(status().isNotFound());
        //then
    }

    @Test
    public void searchAvailableHotelsInCountry_happyPath() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenExistingUser = jwtHelper.createToken(existingUser.getEmail());

        CountryEntity countryToSearchFreeHotels = dbCountryHelper.countryToDb();

        HotelEntity hotelWithFreeRooms1 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity freeRoomInHotel1 = dbRoomHelper.roomToDb(hotelWithFreeRooms1, NumberOfSeatsAndFactor.ONE_SEAT);

        HotelEntity hotelWithFreeRooms2 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity freeRoomInHotel2 = dbRoomHelper.roomToDb(hotelWithFreeRooms2, NumberOfSeatsAndFactor.ONE_SEAT);

        HotelEntity hotelWithFreeRooms3 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity freeRoomInHotel3 = dbRoomHelper.roomToDb(hotelWithFreeRooms3, NumberOfSeatsAndFactor.ONE_SEAT);

        Date startDate = Date.valueOf(String.valueOf((LocalDate.now().plus(1, ChronoUnit.DAYS))));
        Date endDate = Date.valueOf(String.valueOf(LocalDate.now().plus(5, ChronoUnit.DAYS)));

        UserEntity userWhoOccupiedRooms = dbUserHelper.userToDb(UserRole.USER);

        HotelEntity hotelWithNotFreeRooms1 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity notFreeRoomHotel1 = dbRoomHelper.roomToDb(hotelWithNotFreeRooms1, NumberOfSeatsAndFactor.ONE_SEAT);
        BookingEntity bookingUsersWhoOccupiedTheRooms1 = dbBookingHelper
                .bookingToDb(userWhoOccupiedRooms, notFreeRoomHotel1, startDate, endDate);

        HotelEntity hotelWithNotFreeRooms2 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity notFreeRoomHotel2 = dbRoomHelper.roomToDb(hotelWithNotFreeRooms2, NumberOfSeatsAndFactor.ONE_SEAT);
        BookingEntity bookingUsersWhoOccupiedTheRooms2 = dbBookingHelper
                .bookingToDb(userWhoOccupiedRooms, notFreeRoomHotel2, startDate, endDate);
        //when
        mockMvc.perform(request(GET
                        , "/countries/" + countryToSearchFreeHotels.getId() + "/searchAvailableHotels")
                        .header("Content-Type", "application/json")
                        .header("Authorization", tokenExistingUser)
                        .param("numberOfSeatsAndFactor", String.valueOf(NumberOfSeatsAndFactor.ONE_SEAT))
                        .param("startDate", String.valueOf(startDate))
                        .param("endDate", String.valueOf(endDate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
        //then
    }

    @Test
    public void searchAvailableHotelsInCountry_whenStartDateEarlierThanNow() throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenExistingUser = jwtHelper.createToken(existingUser.getEmail());

        CountryEntity countryToSearchFreeHotels = dbCountryHelper.countryToDb();

        HotelEntity hotelWithFreeRooms1 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity freeRoomInHotel1 = dbRoomHelper.roomToDb(hotelWithFreeRooms1, NumberOfSeatsAndFactor.ONE_SEAT);

        HotelEntity hotelWithFreeRooms2 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity freeRoomInHotel2 = dbRoomHelper.roomToDb(hotelWithFreeRooms2, NumberOfSeatsAndFactor.ONE_SEAT);

        HotelEntity hotelWithFreeRooms3 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity freeRoomInHotel3 = dbRoomHelper.roomToDb(hotelWithFreeRooms3, NumberOfSeatsAndFactor.ONE_SEAT);

        Date startDate = Date.valueOf(String.valueOf((LocalDate.now().minus(1, ChronoUnit.DAYS))));
        Date endDate = Date.valueOf(String.valueOf(LocalDate.now().plus(5, ChronoUnit.DAYS)));

        UserEntity userWhoOccupiedRooms = dbUserHelper.userToDb(UserRole.USER);

        HotelEntity hotelWithNotFreeRooms1 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity notFreeRoomHotel1 = dbRoomHelper.roomToDb(hotelWithNotFreeRooms1, NumberOfSeatsAndFactor.ONE_SEAT);
        BookingEntity bookingUsersWhoOccupiedTheRooms1 = dbBookingHelper
                .bookingToDb(userWhoOccupiedRooms, notFreeRoomHotel1, startDate, endDate);

        HotelEntity hotelWithNotFreeRooms2 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity notFreeRoomHotel2 = dbRoomHelper.roomToDb(hotelWithNotFreeRooms2, NumberOfSeatsAndFactor.ONE_SEAT);
        BookingEntity bookingUsersWhoOccupiedTheRooms2 = dbBookingHelper
                .bookingToDb(userWhoOccupiedRooms, notFreeRoomHotel2, startDate, endDate);
        //when
        mockMvc.perform(request(GET
                        , "/countries/" + countryToSearchFreeHotels.getId() + "/searchAvailableHotels")
                        .header("Content-Type", "application/json")
                        .header("Authorization", tokenExistingUser)
                        .param("numberOfSeatsAndFactor", String.valueOf(NumberOfSeatsAndFactor.ONE_SEAT))
                        .param("startDate", String.valueOf(startDate))
                        .param("endDate", String.valueOf(endDate)))
                .andExpect(status().isForbidden());
        //then
    }

    @Test
    public void searchAvailableHotelsInCountry_whenStartDateOfTheBookingCannotBeLaterThanTheEndDate()
            throws Exception {
        //given
        UserEntity existingUser = dbUserHelper.userToDb(UserRole.USER);
        String tokenExistingUser = jwtHelper.createToken(existingUser.getEmail());

        CountryEntity countryToSearchFreeHotels = dbCountryHelper.countryToDb();

        HotelEntity hotelWithFreeRooms1 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity freeRoomInHotel1 = dbRoomHelper.roomToDb(hotelWithFreeRooms1, NumberOfSeatsAndFactor.ONE_SEAT);

        HotelEntity hotelWithFreeRooms2 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity freeRoomInHotel2 = dbRoomHelper.roomToDb(hotelWithFreeRooms2, NumberOfSeatsAndFactor.ONE_SEAT);

        HotelEntity hotelWithFreeRooms3 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity freeRoomInHotel3 = dbRoomHelper.roomToDb(hotelWithFreeRooms3, NumberOfSeatsAndFactor.ONE_SEAT);

        Date startDate = Date.valueOf(String.valueOf((LocalDate.now().plus(10, ChronoUnit.DAYS))));
        Date endDate = Date.valueOf(String.valueOf(LocalDate.now().plus(5, ChronoUnit.DAYS)));

        UserEntity userWhoOccupiedRooms = dbUserHelper.userToDb(UserRole.USER);

        HotelEntity hotelWithNotFreeRooms1 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity notFreeRoomHotel1 = dbRoomHelper.roomToDb(hotelWithNotFreeRooms1, NumberOfSeatsAndFactor.ONE_SEAT);
        BookingEntity bookingUsersWhoOccupiedTheRooms1 = dbBookingHelper
                .bookingToDb(userWhoOccupiedRooms, notFreeRoomHotel1, startDate, endDate);

        HotelEntity hotelWithNotFreeRooms2 = dbHotelHelper.hotelToDb(countryToSearchFreeHotels);
        RoomEntity notFreeRoomHotel2 = dbRoomHelper.roomToDb(hotelWithNotFreeRooms2, NumberOfSeatsAndFactor.ONE_SEAT);
        BookingEntity bookingUsersWhoOccupiedTheRooms2 = dbBookingHelper
                .bookingToDb(userWhoOccupiedRooms, notFreeRoomHotel2, startDate, endDate);
        //when
        mockMvc.perform(request(GET
                        , "/countries/" + countryToSearchFreeHotels.getId() + "/searchAvailableHotels")
                        .header("Content-Type", "application/json")
                        .header("Authorization", tokenExistingUser)
                        .param("numberOfSeatsAndFactor", String.valueOf(NumberOfSeatsAndFactor.ONE_SEAT))
                        .param("startDate", String.valueOf(startDate))
                        .param("endDate", String.valueOf(endDate)))
                .andExpect(status().isForbidden());
        //then
    }
}
